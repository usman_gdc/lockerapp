//
//  ViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 03/06/2021.
//

import UIKit
import iTunesSearchAPI
import StoreKit

class ViewController: UIViewController, SKStoreProductViewControllerDelegate {
    

    let itunes: iTunes = iTunes(session: URLSession.shared, debug: true)
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    func printJSON(json: Any) {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let appDetails = try! JSONDecoder().decode(ITuneAppsResponeModel.self, from: jsonData)
            print(appDetails)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func galleryBtn(_ sender: Any) {
        
        _ = itunes.search(for: "facebook", ofType: .software(.iPadSoftware), options: nil) { (result) in
            guard let value = result.value else {
                return
            }
            self.printJSON(json: value)
        }
    }
    
    
    
    
    
    @IBAction func nextBtn(_ sender: Any) {
        
        
        
    }
    
    func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        //        https://itunes.apple.com/in/app/<AppName>/id<AppID>?mt=8
//        "https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
        
        if let url = URL(string: "https://itunes.apple.com/in/app/Messenger/id454638411?mt=8")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }

    }
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

    
    
    
}
    
// Codable



