//
//  OldPasswordViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 16/06/2021.
//

import UIKit

class OldPasswordViewController: BaseViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var dotOne: UIView!
    @IBOutlet weak var dotTwo: UIView!
    @IBOutlet weak var dotThree: UIView!
    @IBOutlet weak var dotFour: UIView!
    @IBOutlet weak var dotFive: UIView!
    @IBOutlet weak var dotSix: UIView!
    
    @IBOutlet weak var labelIncorrectPass: UILabel!
    
    
    @IBOutlet weak var textPassword: UITextField!{
        didSet {
            textPassword.delegate = self
        }
    }
    
    //MARK:- Properties
    
    
    var oldPassword: String = ""
    var password: String = ""
    var showErrorMsg: Bool = false {
        didSet {
            labelIncorrectPass.isHidden = !self.showErrorMsg
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textPassword.becomeFirstResponder()
        setBackButton()
    }
    
    //MARK:- Actions
    
    @IBAction func btnCancel(_ sender: Any) {
        
        clear()
    }
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func clear(){
        password = ""
        textPassword.text = ""
        dotOne.backgroundColor = UIColor.Color.lightGrey
        dotTwo.backgroundColor = UIColor.Color.lightGrey
        dotThree.backgroundColor = UIColor.Color.lightGrey
        dotFour.backgroundColor = UIColor.Color.lightGrey
        dotFive.backgroundColor = UIColor.Color.lightGrey
        dotSix.backgroundColor = UIColor.Color.lightGrey
        
    }
    
    //MARK:- Router
    func enterPasswordViewController() {
        let vc: EnterPasswordViewController = EnterPasswordViewController.initiateFrom(Storybaord: .main)
        
        push(toController: vc, animated: true)
        
    }

}

//MARK:- UITextFieldDelegate
extension OldPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == "") {
            clear()
            return true
        }
        
        let count = textField.text!.count
        switch count {
        case 0:
            dotOne.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 1:
            dotTwo.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 2:
            dotThree.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 3:
            dotFour.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 4:
            dotFive.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        default:
            dotSix.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
            
            if oldPassword == password{
                print("Success")
                enterPasswordViewController()
            //    passcodeViewController()
            }
            else{
                print("Wrong Password")
                showErrorMsg = true
            }
            
        }
        return count <= 5
    }
    
    

}
