//
//  FaceIDViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 10/08/2021.
//

import UIKit

class FaceIDViewController: BaseViewController {

    //MARK:- Properties
    var isLockSuccessful: (_ isSuccess: Bool) -> Void = { (_ isSuccess: Bool) in
    }
    var showCancelBtn: Bool = false
    var userRatedAppFaceIdCheck:Int = 1
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        if showCancelBtn {
            setCancelBtn()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    //MARK:- Actions
    
    @IBAction func btnFaceID(_ sender: Any) {
        showFaceIdAuthentication { (success) in
            DispatchQueue.main.async { [unowned self] in
                guard success else{ return }
                UserDefaults.standard.appPassCode = AppPassCodeModel(passCode: nil, passCodeType: .faceId)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    userRatedAppFaceIdCheck = UserDefaults.standard.userRatedAppFaceId
                    if userRatedAppFaceIdCheck % 2 == 0 //|| userRatedAppFaceIdCheck == 1
                    {
                        UserDefaults.standard.userRatedAppFaceId += 1
                        RateAndReviewApp.rateApp()
                        self.dismiss(animated: true) { [unowned self] in
                            self.isLockSuccessful(true)
                        }
                    }
                    else{
                        UserDefaults.standard.userRatedAppFaceId += 1
                        self.dismiss(animated: true) { [unowned self] in
                            self.isLockSuccessful(true)
                        }
                    }
                    
                }
            }
        }

    }
    //MARK:- Setter
    func setCancelBtn() {
        setLeftBarButton(WithTitle: "Cancel", selector: #selector(cancelBtnDismiss))
    }
    
    //MARK:- Helping Methods
       
       @objc func cancelBtnDismiss(){
           self.dismiss()
       }
    
    //MARK:- Router

}
