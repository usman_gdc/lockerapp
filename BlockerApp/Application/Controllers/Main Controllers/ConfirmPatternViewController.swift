//
//  ConfirmPatternViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 16/06/2021.
//

import UIKit
import LocalAuthentication
import CCGestureLock

class ConfirmPatternViewController: BaseViewController {
    
    //MARK:- IBOutlets  labelIncorrectPass
    @IBOutlet weak var viewPatternLock: CCGestureLock!
    
    //MARK:- Properties
    
    var confirmPattern: String = ""
    
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        setPatternTarget()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    //MARK:- Actions
    
    @objc func gestureComplete(gestureLock: CCGestureLock) {
        
        if isPatternMatchWith(Sequence: gestureLock.lockSequence) {
            self.setPatternLockFrom(pattern: gestureLock.lockSequence)
            self.dismiss(animated: true)
            
        }
        else {
            self.showAlertWithTitle(message: "Pattern not matched. Please try again!") {
                self.viewPatternLock.gestureLockState = .normal
            }
        }
    }
    
    
    //MARK:- Setter
    
    func setPatternLockFrom(pattern: [NSNumber]) {
        let sequenceString = pattern.map { (number) -> String in
            return "\(number)"
        }.joined()
        UserDefaults.standard.appPassCode = AppPassCodeModel(passCode: sequenceString, passCodeType: .pattern)
    }
    
    func setPatternTarget() {
        viewPatternLock.addTarget(self, action:#selector(gestureComplete(gestureLock:)), for: .gestureComplete)
        viewPatternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 10,
            color: .white,
            forState: .normal
        )
        viewPatternLock.setSensorAppearance(
            type: .outer,
            color: .clear,
            forState: .normal
        )
        // Sensor point customisation (selected)
        viewPatternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 10,
            color: .white,
            forState: .selected
        )
        viewPatternLock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 0,
            color: .clear,
            forState: .selected
        )
        
        [CCGestureLock.GestureLockState.normal, CCGestureLock.GestureLockState.selected].forEach { (state) in
            viewPatternLock.setLineAppearance(
                width: 2,
                color: UIColor.white,
                forState: state
            )
        }
        
    }
    
    //MARK:- Getter
    
    
    //MARK:- Helping Methods
    
    func isPatternMatchWith(Sequence sequence: [NSNumber] ) -> Bool {
        
        let sequenceString = sequence.map { (number) -> String in
            return "\(number)"
        }.joined()
        
        if confirmPattern == sequenceString {
            return true
        }
        return false
        
        
    }
    
    //MARK:- Router
    
    func settingsViewController() {
        let vc: SettingsViewController = SettingsViewController.initiateFrom(Storybaord: .settings)
        push(toController: vc, animated: true)
    }
    
}
