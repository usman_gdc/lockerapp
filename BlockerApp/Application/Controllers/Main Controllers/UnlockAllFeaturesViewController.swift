//
//  UnlockAllFeaturesViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 25/08/2021.
//

import GoogleMobileAds
import StoreKit
import UIKit



class UnlockAllFeaturesViewController: BaseViewController {
    
    enum AdType {
        case reward
        case rate
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!
    
    //MARK:- Properties
    var selectedOption = Int()
    var clicked:Bool = false
    
    
    var selectedAdType: (_ type: AdType) -> Void = { (type: AdType) in
        
    }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.makeTransparent()
        checkUserAlreadyRateTheApp()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- Actions
    
    func dismissWithAdtype(type: AdType) {
        self.dismiss(animated: true) {
            self.selectedAdType(type)
        }
    }
    
    @IBAction func btnDismiss(_ sender: Any) {
        self.dismiss()
    }
    
    @IBAction func btnShowVideoAds(_ sender: Any) {
        dismissWithAdtype(type: .reward)
    }
    
    @IBAction func btnRatingFeedback(_ sender: Any) {
        dismissWithAdtype(type: .rate)
    }
    
    //MARK:- Helping Methods
    
    fileprivate func checkUserAlreadyRateTheApp() {
        if UserDefaults.standard.userRatedApp {
            viewRating.isHidden = true
            viewBottomHeight.constant = 180
        }
    }
}
