//
//  PasscodeViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 11/06/2021.
//

import AppTrackingTransparency
import AdSupport
import GoogleMobileAds
import UIKit
import LocalAuthentication
import CCGestureLock

class PasscodeViewController: BaseViewController {
    
    //MARK:- IBOutlets
    
    
    
    @IBOutlet weak var dotOne: UIView!
    @IBOutlet weak var dotTwo: UIView!
    @IBOutlet weak var dotThree: UIView!
    @IBOutlet weak var dotFour: UIView!
    @IBOutlet weak var dotFive: UIView!
    @IBOutlet weak var dotSix: UIView!
    @IBOutlet weak var labelIncorrectPass: UILabel!
    @IBOutlet weak var textPassword: UITextField!{
        didSet {
            textPassword.delegate = self
        }
    }
    
    //MARK:- Properties
    
    var password: String = ""
    var showBackBtnPasscode: Bool = false
    var showErrorMsg: Bool = false {
        didSet {
            labelIncorrectPass.isHidden = !self.showErrorMsg
        }
    }
    var userRatedAppPasswordCheck:Int = 1
    
    var isLockSuccessful: (_ isSuccess: Bool) -> Void = { (_ isSuccess: Bool) in
    }
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showErrorMsg = false
        setBackButton()
        if showBackBtnPasscode == true {
            showBackBtn()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textPassword.becomeFirstResponder()
//        if userRatedAppPasswordCheck > 1 {
//        userRatedAppPasswordCheck = UserDefaults.standard.userRatedAppPassword
//        }
    }
    
    override func applicationDidBecomeActive() {
        
    }
    
    //MARK:- Actions
    @IBAction func btnPattern(_ sender: Any) {
        patternViewController()
    }
    
    @IBAction func btnFaceId(_ sender: Any) {
        showFaceIdAuthentication { (success) in
            DispatchQueue.main.async { [unowned self] in
                guard success else{ return }
                
                UserDefaults.standard.appPassCode = AppPassCodeModel(passCode: nil, passCodeType: .faceId)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.dismissViewController()
                }
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        clear()
    }
    
    
    //MARK:- Setter
    func showBackBtn(){
        setLeftBarButton(WithTitle: "Cancel", selector: #selector(backBtnDismiss))
        
    }
    
   //MARK:- Getter

    //MARK:- Helping Methods

   
    
    
    @objc func backBtnDismiss(){
        self.dismiss(animated: true)
    }
    
    func clear(){
        password = ""
        textPassword.text = ""
        dotOne.backgroundColor = UIColor.Color.lightGrey
        dotTwo.backgroundColor = UIColor.Color.lightGrey
        dotThree.backgroundColor = UIColor.Color.lightGrey
        dotFour.backgroundColor = UIColor.Color.lightGrey
        dotFive.backgroundColor = UIColor.Color.lightGrey
        dotSix.backgroundColor = UIColor.Color.lightGrey
        
    }
    
    //MARK:- Router

    func patternViewController() {
        let vc: PatternViewController = PatternViewController.initiateFrom(Storybaord: .main)
        vc.isLockSuccessful = { [unowned self] (isSuccess) in
            self.dismissViewController()
        }
        self.push(toController: vc)
    }
    
    func dismissViewController() {
        self.dismiss(animated: true) { [unowned self] in
            self.isLockSuccessful(true)
        }
    }
}

//MARK:- UITextFieldDelegate
extension PasscodeViewController: UITextFieldDelegate {
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == "") {
            clear()
            return true
        }
        
        let count = textField.text!.count
        switch count {
        case 0:
            dotOne.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 1:
            dotTwo.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 2:
            dotThree.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 3:
            dotFour.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 4:
            dotFive.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        default:
            dotSix.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
            
            if UserDefaults.standard.appPassCode?.passCode == password {
                userRatedAppPasswordCheck = UserDefaults.standard.userRatedAppPassword
                if userRatedAppPasswordCheck % 2 == 0 //|| userRatedAppPasswordCheck == 1
                {
                    UserDefaults.standard.userRatedAppPassword += 1
                    RateAndReviewApp.rateApp()
                    self.dismiss(animated: true){ [unowned self] in
                        self.isLockSuccessful(true)
                    }

                }
                else{
                    UserDefaults.standard.userRatedAppPassword += 1
                    self.dismiss(animated: true){ [unowned self] in
                        self.isLockSuccessful(true)
                    }
                }
            }
            else{
                showErrorMsg = true
            }
        }
        return count <= 5
    }
    
   
    

}

