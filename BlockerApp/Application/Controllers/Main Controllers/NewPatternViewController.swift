//
//  NewViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 16/06/2021.
//

import UIKit
import LocalAuthentication
import CCGestureLock


class NewPatternViewController: BaseViewController {

    //MARK:- IBOutlets
  
    @IBOutlet weak var viewPatternLock: CCGestureLock!
    
    //MARK:- Properties

    var newPattern : String = ""
    var showCancelBtn: Bool = false
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        setPatternTarget()
        if showCancelBtn {
            setCancelBtn()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        viewPatternLock.gestureLockState = .normal
    }
    
    
    //MARK:- Actions
    
    @objc func gestureComplete(gestureLock: CCGestureLock) {
        
        if isPatternMatchWith(Sequence: gestureLock.lockSequence) {
        
        } else {
            self.showAlertWithTitle(message: "Pattern not matched. Please try again!") {
                self.viewPatternLock.gestureLockState = .normal
            }
        }
    }
    
    @objc func dismissNow() {
        self.dismiss()
    }
    
    
    
    //MARK:- Setter
    
    func setPatternTarget() {
        viewPatternLock.addTarget(self, action: #selector(gestureComplete(gestureLock:)), for: .gestureComplete)
        viewPatternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 10,
            color: .white,
            forState: .normal
        )
        viewPatternLock.setSensorAppearance(
            type: .outer,
            color: .clear,
            forState: .normal
        )
        // Sensor point customisation (selected)
        viewPatternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 10,
            color: .white,
            forState: .selected
        )
        viewPatternLock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 0,
            color: .clear,
            forState: .selected
        )
        
        [CCGestureLock.GestureLockState.normal, CCGestureLock.GestureLockState.selected].forEach { (state) in
            viewPatternLock.setLineAppearance(
                 width: 2,
                 color: UIColor.white,
                 forState: state
             )
        }

    }
    
    func setCancelBtn() {
        setLeftBarButton(WithTitle: "Cancel", selector: #selector(cancelBtnDismiss))
    }
    
   //MARK:- Getter

    
   //MARK:- Helping Methods
    
    @objc func cancelBtnDismiss(){
        self.dismiss()
    }
    
    func isPatternMatchWith(Sequence sequence: [NSNumber] ) -> Bool {
        
         newPattern = sequence.map { (number) -> String in
            return "\(number)"
        }.joined()
        confirmPatternViewController()
        return true
    }
//        guard let _appPassCode = UserDefaults.standard.appPassCode, let _passCode = _appPassCode.passCode, _passCode == sequenceString else {
//            return false
//        }
//        return true
        
    
    
    //MARK:- Router

    func confirmPatternViewController() {
        let vc: ConfirmPatternViewController = ConfirmPatternViewController.initiateFrom(Storybaord: .main)
        
        vc.confirmPattern = newPattern
        push(toController: vc, animated: true)
    }

}
