//
//  HomeViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 11/06/2021.
//
import GoogleMobileAds
import UIKit

class HomeViewController: BaseViewController {
    
    //MARK:- ENUM
    enum HomeType {
        case photo
        case video
        case browser
        case apps
        case files
        case notes
        case contacts
        
        var iconName: String {
            switch self {
            case .photo:
                guard UserDefaults.standard.isPhotosUnlock else {
                    return "photosAd" 
                }
                return "Photos"
            case .video:
                guard UserDefaults.standard.isVideosUnlock else {
                    return "videosAd"
                }
                return  "Video"
            case .browser:
                guard UserDefaults.standard.isBrowserUnlock else {
                    return "browserAd"
                }
                return "Browser"
            case .apps:
                return "App"
            case .files:
                return "Files"
            case .notes:
                return "Note"
            case .contacts:
                return "Contact"
            }
        }
        
    }
    enum AdDecisionPolicy {
        case show
        case hide
        case doNothing
    }
    
    //MARK:- typealias
    typealias HomeDataSource = (iconName: String, type: HomeType)
    
    //MARK:- IBOutlets
    @IBOutlet weak var viewBannerAd: GADBannerView!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    //MARK:- Properties
    var dataSource: [HomeType] = [.photo, .video, .browser, .apps, .files, .notes, .contacts]
    
    var rewardAdAndReviewAppCount = 1
    var rewardAndReviewPolicy: AdDecisionPolicy = .show
    var interstitialPolicy: AdDecisionPolicy = .show
    var adMobManager = AdMobManager()
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self
        homeCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        homeCollectionView.register(UINib(nibName: "HomeAdCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeAdCollectionViewCell")
        checkAppPassCode()
        loadBannerAd()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
        homeCollectionView.reloadData()
    }
    
    //MARK:- Actions
    
    @IBAction func btnUnlockSettings(_ sender: Any) {
        
        settingsViewController()
    }
    
    //MARK:- Helping Method
    
    func loadBannerAd() {
        adMobManager.loadBannerAd(viewBannerAd: viewBannerAd, in: self)
    }
    
    func checkPhotodAreLockedOrNot() {
        guard UserDefaults.standard.isPhotosUnlock else {
            showUnlockAllFeaturesViewController { [unowned self] (isComplete) in
                
                if isComplete {
                    UserDefaults.standard.isPhotosUnlock = true
                    self.checkPhotodAreLockedOrNot()
                }
            }
            return
        }
        photoViewController()
    }
    
    func checkVideosAreLockedOrNot() {
        guard UserDefaults.standard.isVideosUnlock else {
            showUnlockAllFeaturesViewController { [unowned self] (isComplete) in
                
                if isComplete {
                    UserDefaults.standard.isVideosUnlock = true
                    self.checkVideosAreLockedOrNot()
                }
            }
            return
        }
        videoViewController()
    }
    
    func checkBrowserAreLockedOrNot() {
        guard UserDefaults.standard.isBrowserUnlock else {
            showUnlockAllFeaturesViewController { [unowned self] (isComplete) in
                
                if isComplete {
                    UserDefaults.standard.isBrowserUnlock = true
                    self.checkBrowserAreLockedOrNot()
                }
            }
            return
        }
        webBrowserViewController()
    }
    
    func showControllerRelatedToIndexPath(indexPath: IndexPath) {
        switch dataSource[indexPath.row] {
        case .photo:
            photoViewController()
            break
        case .video:
            videoViewController()
            break
        case .browser:
            webBrowserViewController()
            break
        case .apps:
            appViewController()
            break
        case .files:
            foldersViewController()
            break
        case .notes:
            noteViewController()
            break
        case .contacts:
            contactViewController()
            break
        }
    }
    
    func didSelectOptionAt(indexPath: IndexPath) {
        
        switch dataSource[indexPath.row] {
        case .photo:
            checkPhotodAreLockedOrNot()
            break
        case .video:
            checkVideosAreLockedOrNot()
            break
        case .browser:
            checkBrowserAreLockedOrNot()
            break
        case .apps, .files, .notes, .contacts:
            switch interstitialPolicy {
            case .show:
                showInterstitialAd { [unowned self] (isComplete) in
                    guard isComplete else {
                        showAlertWithTitle(message: "Reward video might be cancel by the user or video might not show due to some problem") {
                        }
                        return
                    }
                    self.interstitialPolicy = .doNothing
                    didSelectOptionAt(indexPath: indexPath)

                }
                return
            case .doNothing:
                interstitialPolicy = .hide
                self.showControllerRelatedToIndexPath(indexPath: indexPath)
                return
            case .hide:
                interstitialPolicy = .show
                self.showControllerRelatedToIndexPath(indexPath: indexPath)
                return
            }
            
        }
    }
    
    func didSelectOptionAt1(indexPath: IndexPath) {
        
        switch dataSource[indexPath.row] {
        case .photo, .video, .browser:
            
            switch rewardAndReviewPolicy {
            case .show:
                showUnlockAllFeaturesViewController { [unowned self] (isComplete) in
                    guard isComplete else {
                        showAlertWithTitle(message: "Reward video might be cancel by the user or video might not show due to some problem") {
                        }
                        return
                    }
                    self.rewardAndReviewPolicy = .doNothing
                    didSelectOptionAt(indexPath: indexPath)
                }
                return
            case .doNothing:
                rewardAndReviewPolicy = .hide
                self.showControllerRelatedToIndexPath(indexPath: indexPath)
                return
            case .hide:
                rewardAndReviewPolicy = .show
                self.showControllerRelatedToIndexPath(indexPath: indexPath)
                return
            }
        case .apps, .files, .notes, .contacts:
            switch interstitialPolicy {
            case .show:
                showInterstitialAd { [unowned self] (isComplete) in
                    guard isComplete else {
                        showAlertWithTitle(message: "Reward video might be cancel by the user or video might not show due to some problem") {
                        }
                        return
                    }
                    self.interstitialPolicy = .doNothing
                    didSelectOptionAt(indexPath: indexPath)

                }
                return
            case .doNothing:
                interstitialPolicy = .hide
                self.showControllerRelatedToIndexPath(indexPath: indexPath)
                return
            case .hide:
                interstitialPolicy = .show
                self.showControllerRelatedToIndexPath(indexPath: indexPath)
                return
            }
            
        }
    }
    
    //MARK:- Router
    
    func showInterstitialAd(completion: @escaping(_ isComplete: Bool) -> Void) {
        adMobManager.loadInterstitialAd(from: self) { (isComplete, value) in
            completion(isComplete)
        }
    }
    
    func showRewardedAd(completion: @escaping(_ isComplete: Bool) -> Void) {
        adMobManager.loadRewardedAd(in: self) { (isComplete, rewardValue) in
            guard let _ = rewardValue else {
                completion(false)
                return
            }
            completion(isComplete)
        }
    }
    
    func showUnlockAllFeaturesViewController(completion: @escaping(_ isComplete: Bool) -> Void) {
        let controller = UnlockAllFeaturesViewController.initiateFrom(Storybaord: .main)
        controller.selectedAdType = { [unowned self] (adType: UnlockAllFeaturesViewController.AdType) in
            switch adType {
            case .rate:
                RateAndReviewApp.reviewApp()
                UserDefaults.standard.userRatedApp = true
                completion(true)
                break
            case .reward:
                self.showRewardedAd { (isComplete) in
                    completion(isComplete)
                }
                break
            }
        }
        
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .overCurrentContext
        navController.modalTransitionStyle = .crossDissolve
        self.present(navController, animated: true) {
            
        }
    }
    
    func photoViewController() {
        let vc: PhotoViewController = PhotoViewController.initiateFrom(Storybaord: .media)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func videoViewController() {
        let vc: VideoViewController = VideoViewController.initiateFrom(Storybaord: .media)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func webBrowserViewController() {
        let vc: WebBrowserViewController = WebBrowserViewController.initiateFrom(Storybaord: .browser)
        push(toController: vc, animated: true)
    }
    
    func settingsViewController() {
        let vc: SettingsViewController = SettingsViewController.initiateFrom(Storybaord: .settings)
        push(toController: vc, animated: true)
    }
    
}
//MARK:- Extensions

extension HomeViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectOptionAt(indexPath: indexPath)
    }
}

extension HomeViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.imageView.image = UIImage(named: dataSource[indexPath.row].iconName)
        return cell
    }
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.size.width - 90
        return CGSize(width: width/3, height: width/3)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        //vertical padding
        return 38
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        //horizontal padding
        return 13
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 40, left: 25, bottom: 0, right: 25)
    }
}
