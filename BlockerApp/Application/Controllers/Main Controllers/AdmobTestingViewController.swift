////
////  AdmobTestingViewController.swift
////  BlockerApp
////
////  Created by IncubatorMac01 on 24/08/2021.
////
//
//import GoogleMobileAds
//import StoreKit
//import UIKit
//
//class AdmobTestingViewController: BaseViewController {
//    
//    
//    var interstitial: GADInterstitialAd?
//    var rewardedAd: GADRewardedAd?
//    var coinCount = 0
//    var gotReward = 0
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        appViewController()
//        //loadInterstitial()
////        loadRewardedAd()
//    }
//    func appViewController() {
//        let vc: AppViewController = AppViewController.initiateFrom(Storybaord: .browser)
//        push(toController: vc, animated: true)
//    }
//
//    
//    func loadInterstitial() {
//        let request = GADRequest()
//        GADInterstitialAd.load(withAdUnitID:"ca-app-pub-3940256099942544/4411468910",
//                               request: request,
//                               completionHandler: { [self] ad, error in
//                                if let error = error {
//                                    print("Failed to load interstitial ad with error: \(error.localizedDescription)")
//                                    return
//                                }
//                                interstitial = ad
//                                interstitial?.fullScreenContentDelegate = self
//                                interstitial?.present(fromRootViewController: self)
//                               })
//        
//    }
//    
//    func loadRewardedAd(){
//        GADRewardedAd.load(
//          withAdUnitID: "ca-app-pub-3940256099942544/1712485313", request: GADRequest()
//        ) { (ad, error) in
//          if let error = error {
//            print("Rewarded ad failed to load with error: \(error.localizedDescription)")
//            return
//          }
//          print("Loading Succeeded")
//            self.rewardedAd = ad
//            self.rewardedAd?.fullScreenContentDelegate = self
//            
//            self.btnClick()
////            self.rewardedAd?.present(fromRootViewController: self, userDidEarnRewardHandler: {
////                let reward =  self.rewardedAd?.adReward
////                self.getReward(NSInteger(truncating: reward?.amount ?? 0))
////            })
//        }
//        
//    }
//    
//    
//    fileprivate func getReward(_ reward: NSInteger) {
//        gotReward = reward
//      print("Reward: \(self.gotReward)")
//    }
//    
//    fileprivate func earnCoins(_ coins: NSInteger) {
//      coinCount += coins
//      print("Coins: \(self.coinCount)")
//    }
//    
//    
//    func btnClick(){
//        print("btnClicked")
//        
//        guard let _ad = self.rewardedAd else {
//            let alert = UIAlertController(
//              title: "Rewarded ad isn't available yet.",
//              message: "The rewarded ad cannot be shown at this time",
//              preferredStyle: .alert)
//            let alertAction = UIAlertAction(
//              title: "OK",
//              style: .cancel,
//              handler: { [weak self] action in
//                self?.loadRewardedAd()
//              })
//            alert.addAction(alertAction)
//            self.present(alert, animated: true, completion: nil)
//            return
//          }
//        _ad.present(fromRootViewController: self, userDidEarnRewardHandler: {
//                let reward =  self.rewardedAd?.adReward
//                self.getReward(NSInteger(truncating: reward?.amount ?? 0))
//            print(self.gotReward)
//            })
//        return
//        
//    }
//    
//    
//}
//
//
//
//// MARK: - GADFullScreenContentDelegate
//
//extension AdmobTestingViewController: GADFullScreenContentDelegate {
//    
//    /// Tells the delegate that the ad failed to present full screen content.
//      func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
//        print("Ad did fail to present full screen content.")
//      }
//
//      /// Tells the delegate that the ad presented full screen content.
//      func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("Ad did present full screen content.")
//      }
//
//      /// Tells the delegate that the ad dismissed full screen content.
//      func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("Ad did dismiss full screen content.")
////        GADInterstitialAd is a one-time-use object. This means that once an interstitial ad is shown, it cannot be shown again. A best practice is to load another interstitial ad in the adDidDismissFullScreenContent: method on GADFullScreenContentDelegate so that the next interstitial ad starts loading as soon as the previous one is dismissed.
//      }
//    
//}
