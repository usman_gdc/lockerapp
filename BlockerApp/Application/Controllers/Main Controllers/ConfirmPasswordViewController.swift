//
//  ConfirmPasswordViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 10/06/2021.
//

import UIKit

class ConfirmPasswordViewController: BaseViewController {
    
    //MARK:- IBOutlets
    
    
    @IBOutlet weak var labelIncorrectPass: UILabel!
    @IBOutlet weak var dotOne: UIView!
    @IBOutlet weak var dotTwo: UIView!
    @IBOutlet weak var dotThree: UIView!
    @IBOutlet weak var dotFour: UIView!
    @IBOutlet weak var dotFive: UIView!
    @IBOutlet weak var dotSix: UIView!
    @IBOutlet weak var textPassword: UITextField! {
        didSet {
            textPassword.delegate = self
        }
    }
    
    //MARK:- Properties
    var oldPassword: String = ""
    var password: String = ""
    var showErrorMsg: Bool = false {
        didSet {
            labelIncorrectPass.isHidden = !self.showErrorMsg
        }
        
    }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showErrorMsg = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textPassword.becomeFirstResponder()
        setBackButton()
    }

    //MARK:- Actions
    
    @IBAction func btnCancel(_ sender: Any) {
        clear()
    }
    
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func clear(){
        password = ""
        textPassword.text = ""
        dotOne.backgroundColor = UIColor.Color.lightGrey
        dotTwo.backgroundColor = UIColor.Color.lightGrey
        dotThree.backgroundColor = UIColor.Color.lightGrey
        dotFour.backgroundColor = UIColor.Color.lightGrey
        dotFive.backgroundColor = UIColor.Color.lightGrey
        dotSix.backgroundColor = UIColor.Color.lightGrey
        
    }
    
    
    //MARK:- Router
    func dismissHomeViewController() {
        self.dismiss(animated: true) {
            
        }
    }
    
}

//MARK:- UITextFieldDelegate
extension ConfirmPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == "") {
            clear()
            return true
        }
        
        let count = textField.text!.count
        switch count {
        case 0:
            dotOne.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 1:
            dotTwo.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 2:
            dotThree.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 3:
            dotFour.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        case 4:
            dotFive.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
        default:
            dotSix.backgroundColor = UIColor.Color.darkGrey
            password = "\(password)\(string)"
            
            if oldPassword == password {
                UserDefaults.standard.appPassCode = AppPassCodeModel(passCode: password, passCodeType: .passCode)
                UserDefaults.standard.firstSignUp = true
                self.dismiss(animated: true)
            }
            else{
                showErrorMsg = true
            }
            
        }
        return count <= 5
    }
    

    
}

