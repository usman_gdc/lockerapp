//
//  PatternViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 16/06/2021.
//

import UIKit
import CCGestureLock

class PatternViewController: BaseViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var viewPatternLock: CCGestureLock!
    
    //MARK:- Properties
    var isLockSuccessful: (_ isSuccess: Bool) -> Void = { (_ isSuccess: Bool) in
    }
    var userRatedAppPatternCheck:Int = 1
    var showCancelBtn: Bool = false
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatternTarget()
        if showCancelBtn {
            setLeftBarButton(WithTitle: "Cancel", selector: #selector(dismissNow))
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
        
    }
    
    //MARK:- Actions
    

    
    @objc func gestureComplete(gestureLock: CCGestureLock) {
        
        if isPatternMatchWith(Sequence: gestureLock.lockSequence) {
            userRatedAppPatternCheck = UserDefaults.standard.userRatedAppPattern
            if userRatedAppPatternCheck % 2 == 0 //|| userRatedAppPatternCheck == 1
            {
                UserDefaults.standard.userRatedAppPattern += 1
                RateAndReviewApp.rateApp()
                self.dismiss(animated: true) { [unowned self] in
                    self.isLockSuccessful(true)
                }
            }
            else{
                UserDefaults.standard.userRatedAppPattern += 1
                self.dismiss(animated: true) { [unowned self] in
                    self.isLockSuccessful(true)
                }
            }
            
            
        } else {
            self.showAlertWithTitle(message: "Pattern not matched. Please try again!") {
                self.viewPatternLock.gestureLockState = .normal
            }
        }
    }
    
    //MARK:- Setter
    
    func setPatternTarget() {
        viewPatternLock.addTarget(self, action:#selector(gestureComplete(gestureLock:)), for: .gestureComplete)
        viewPatternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 10,
            color: .white,
            forState: .normal
        )
        viewPatternLock.setSensorAppearance(
            type: .outer,
            color: .clear,
            forState: .normal
        )
        // Sensor point customisation (selected)
        viewPatternLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 10,
            color: .white,
            forState: .selected
        )
        viewPatternLock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 0,
            color: .clear,
            forState: .selected
        )
        
        [CCGestureLock.GestureLockState.normal, CCGestureLock.GestureLockState.selected].forEach { (state) in
            viewPatternLock.setLineAppearance(
                 width: 2,
                 color: UIColor.white,
                 forState: state
             )
        }

    }
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    @objc func dismissNow() {
        self.dismiss()
    }
    
    func isPatternMatchWith(Sequence sequence: [NSNumber] ) -> Bool {
        
        let sequenceString = sequence.map { (number) -> String in
            return "\(number)"
        }.joined()
        
        guard let _appPassCode = UserDefaults.standard.appPassCode, let _passCode = _appPassCode.passCode, _passCode == sequenceString else {
            return false
        }
        return true
        
    }
    
}


