////
////  WriteReviewViewController.swift
////  BlockerApp
////
////  Created by IncubatorMac01 on 25/08/2021.
////
//
//import UIKit
//
//class WriteReviewViewController: BaseViewController {
//    //MARK:- IBOutlets
//    @IBOutlet weak var txtTitle: UITextField!
//    @IBOutlet weak var txtNote: RSKPlaceholderTextView!
//    @IBOutlet weak var btnStar1: UIButton!
//    @IBOutlet weak var btnStar2: UIButton!
//    @IBOutlet weak var btnStar3: UIButton!
//    @IBOutlet weak var btnStar4: UIButton!
//    @IBOutlet weak var btnStar5: UIButton!
//
//
//    //MARK:- Properties
//    var btnSend = UIBarButtonItem()
//    var stars = Int()
//
//    //MARK:- Life Cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        title = "Write a Review"
//        setNavigationBarSystemGray6()
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .done, target: self, action: #selector(send))
//        navigationItem.setLeftBarButton(btnSend, animated: true)
//        setLeftBarButtonSystemBlue(WithTitle: "Cancel", selector: #selector(cancel))
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        txtNote.placeholder = "Review (Optional)"
//        txtNote.placeholderColor = UIColor.Color.placeHolderTextColor
//        self.txtTitle.becomeFirstResponder()
//    }
//    //MARK:- Actions
////    let boldConfig = UIImage.SymbolConfiguration(weight: .bold)
////    let boldSearch = UIImage(systemName: "search", withConfiguration: boldConfig)
////
////    button.setImage(boldSearch, for: .normal)
//
//
//    @IBAction func btnStar1(_ sender: Any) {
//        btnStar1.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar2.setImage(UIImage(systemName: "star"), for: .normal)
//        btnStar3.setImage(UIImage(systemName: "star"), for: .normal)
//        btnStar4.setImage(UIImage(systemName: "star"), for: .normal)
//        btnStar5.setImage(UIImage(systemName: "star"), for: .normal)
//        stars = 1
//    }
//
//    @IBAction func btnStar2(_ sender: Any) {
//        btnStar1.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar2.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        stars = 2
//    }
//
//    @IBAction func btnStar3(_ sender: Any) {
//        btnStar1.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar2.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar3.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        stars = 3
//
//    }
//
//    @IBAction func btnStar4(_ sender: Any) {
//        btnStar1.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar2.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar3.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar4.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        stars = 4
//    }
//
//    @IBAction func btnStar5(_ sender: Any) {
//        btnStar1.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar2.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar3.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar4.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        btnStar5.setImage(UIImage(systemName: "star.fill"), for: .normal)
//        stars = 5
//    }
//
//
//
//
//    //MARK:- Setter
//
//    //MARK:- Getter
//
//    //MARK:- Helping Methods
//    @objc func cancel (){
//        pop()
//            }
//
//    @objc func send (){
//        //self.dismiss(animated: true)
//        print("Message Sent")
//            }
//
//    //MARK:- Router
//    func dismissHomeViewController() {
//        self.dismiss(animated: true) {
//
//        }
//    }
//
//
//}
