//
//  AddPhotosViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 28/06/2021.
//

import UIKit
import Photos
import PhotosUI

class AddPhotosViewController: BaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "CollectionViewCellAddPhoto", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellAddPhoto")
        }
    }
    @IBOutlet weak var btnAdd: UIButton!
    
    //MARK:- Properties
    var dataSource: [PhotoAssetModel] = []
    let photoManager = PhotoManager()
    let fileManager = FileHadlingManager()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Photo Library")
        setRightBarButton()
        getPhotos()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    //MARK:- Setter
    
    func setRightBarButton() {
        
        self.btnAdd = self.setRightBarButton(WithTitle: "Add", selector: #selector(btnAdd(_:)))
        btnAdd.isEnabled = false
    }
    //MARK:- Getter
    
    func getPhotos() {
        photoManager.photoAuthorization { (status) in
            switch status {
            case .authorized:
                self.photoManager.loadPhotos { [unowned self] (photos) in
                    self.dataSource = photos
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            case .limited:
                DispatchQueue.main.async {
                    self.showPhotoPicker()
                }
            default:
                print("")
            }
        }
    }
    
    func getSelectedPhotos() -> [PhotoAssetModel] {
        let filteredData = dataSource.compactMap { (model) -> PhotoAssetModel? in
            guard let _isSelected = model.isSelected, _isSelected == true else { return nil }
            return model
        }
        return filteredData
    }
    
    //MARK:- Actions
    
    @IBAction func btnAdd(_ sender: UIButton) {
        showSelectedPhotosAlert()
    }
    
    //MARK:- Helping Methods
    
    func copySelectedPhotos(photos: [PhotoAssetModel]) {
        let group = DispatchGroup()
        var photosModel: [PhotoModel] = []
        for photo in photos {
            guard let _asset = photo.asset else { continue }
            group.enter()
            photoManager.getUrlOf(asset: _asset) { [unowned self] (url) in
                guard let _url = url else { return }
                self.fileManager.copyFile(file: _url, to: .photo, byName: "\(photo.assetId).\(_url.pathExtension)")
                let model = PhotoModel()
                model.photoId = UUID().uuidString
                model.path = "\(photo.assetId).\(_url.pathExtension)"
                photosModel.append(model)
                group.leave()
            }
        }
        group.notify(queue: .main) {
            self.savePhotos(photos: photosModel)
        }
    }
    
    func savePhotos(photos: [PhotoModel]) {
        do {
            try CoreDataManager.manager.savePhotos(photos: photos)
        } catch {
            print("Photos not saved")
        }
    }
    
    func deleteSelectedPhotos() {
        let photos = getSelectedPhotos()
        copySelectedPhotos(photos: photos)
        photoManager.deleteAssets(assets: photos) { [unowned self] (isSuccess) in
            DispatchQueue.main.async {
                self.pop(animated: true)
            }
        }
    }
    
    func showSelectedPhotosAlert() {
    //    let alert = UIAlertActionStyleDefault
        let photos = getSelectedPhotos()
        if photos.count > 0 {
        let alert = UIAlertController(title: "", message: "Selected photos will be delete from gallery. Are you sure?", preferredStyle: .alert)
        let actionContinue = UIAlertAction(title: "Continue", style: .default) {[unowned self] (action) in
            self.deleteSelectedPhotos()
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive) {[unowned self] (action) in
            self.dismiss()
        }
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        self.present(alert, animated: true) {
        }
    }
    }
    
    func showPhotoPicker() {
        let photoLibrary = PHPhotoLibrary.shared()
        if #available(iOS 14, *) {
            let configuration = PHPickerConfiguration(photoLibrary: photoLibrary)
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            present(picker, animated: true)
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
}

//MARK:- UICollectionViewDelegate
extension AddPhotosViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dataSource[indexPath.row].isSelected = !(dataSource[indexPath.row].isSelected ?? true)
        let selectedPhotos = getSelectedPhotos()
        btnAdd.isEnabled = (selectedPhotos.count > 0) ? true : false
        collectionView.reloadItems(at: [indexPath])
    }
}

//MARK:- UICollectionViewDataSource
extension AddPhotosViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = dataSource[indexPath.row]
        let cell = CollectionViewCellAddPhoto.collectionView(collectionView, cellForItemAt: indexPath, data: data, photoManager: photoManager)
        return cell
    }
    
}


//MARK:- UICollectionViewDelegateFlowLayout
extension AddPhotosViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension AddPhotosViewController: PHPickerViewControllerDelegate {
    @available(iOS 14, *)
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)
        
        let identifiers = results.compactMap(\.assetIdentifier)
        photoManager.loadPhotosBy(identifiers: identifiers) { [unowned self] (photos) in
            self.dataSource = photos
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}
