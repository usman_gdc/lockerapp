//
//  PhotoViewerViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 26/07/2021.
//
import Photos
import PhotosUI
import UIKit
import ImageSlideshow

class PhotoViewerViewController: BaseViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewBtmHeight: NSLayoutConstraint!
    
    //MARK:- Properties
    var photoModel = PhotoModel()
    let fileManager: FileHadlingManager = FileHadlingManager()
    var alertDeleteImage: UIAlertController?
    var showView : Bool = true
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        imgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        imgView.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setImage()
    }

    //MARK:- Actions
    
    @IBAction func btnShare(_ sender: Any) {
        shareImageFromApp()
    }
    
    @IBAction func btnMove(_ sender: Any) {
        let alert = UIAlertController(title: "Selected Photo will be moved out of the app?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            moveOutPhoto(photo: photoModel)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Are You Sure ?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            deletePhoto(photos: photoModel)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.alertDeleteImage = alert
        self.present(alert, animated: true, completion: nil)
        
    }
    //MARK:- Setter
    fileprivate func setImage() {
        if let _photosUrl = fileManager.photoDirectoryURL {
            imgView.sd_setImage(with: _photosUrl.appendingPathComponent(photoModel.path ?? "")) { (image, error, cache, url) in
        
                }
            }
        }
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        if (showView == true){
            viewBtmHeight.constant = 0
            showView = false
        }
        else{
            viewBtmHeight.constant = 90
            showView = true
        }
        
        }
    
    func deletePhoto(photos: PhotoModel ) {
        fileManager.deleteSingleDirectoryBy(name: photoModel.path!)
        CoreDataManager.manager.deletePhotoById(photoId: photoModel.photoId!)
        viewBottom.isHidden = true
        pop(animated: true)
    }
    
    func shareImageFromApp(){
        var items = [Any]()
        items = [imgView.image!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    
    func moveOutPhoto(photo: PhotoModel) {
        
        guard let _photoUrl = fileManager.photoDirectoryURL else { return }
        let _photo = _photoUrl.appendingPathComponent(photo.path ?? "")
        self.savePhotoToAlbum(_photo) { (error) in
            self.showDeletePhotosAlert()
        }
    }
    
    
    func requestAuthorization(completion: @escaping ()->Void) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization { (status) in
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else if PHPhotoLibrary.authorizationStatus() == .authorized{
            completion()
        }
    }
    
    func savePhotoToAlbum(_ outputURL: URL, _ completion: ((Error?) -> Void)?) {
        requestAuthorization {
            PHPhotoLibrary.shared().performChanges({
                let request = PHAssetCreationRequest.forAsset()
                request.addResource(with: .photo, fileURL: outputURL, options: nil)
            }) { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("Saved successfully")
                    }
                    completion?(error)
                }
            }
        }
    }
    
    func showDeletePhotosAlert() {
        let alert = UIAlertController(title: "Moved successfully", message: "Do you want to delete moved photo?", preferredStyle: .alert)
        let actionContinue = UIAlertAction(title: "Continue", style: .default) {[unowned self] (action) in
            self.deletePhoto(photos: photoModel)
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive) {[unowned self] (action) in
            self.dismiss()
        }
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        self.present(alert, animated: true) {
        }
    }
    
    
    
    //MARK:- Router

}

//MARK:- Extension


