//
//  VideoViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 13/06/2021.
//

import AVKit
import AVFoundation
import UIKit
import PhotosUI
import Photos

class VideoViewController: BaseViewController {
    
    //MARK:- ENUM
   

    //MARK:- IBOutlets
    
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var videoCollectionView: UICollectionView!{
        didSet {
            videoCollectionView.dataSource = self
            videoCollectionView.delegate = self
            videoCollectionView.register(UINib(nibName: "CollectionViewCellAddVideos", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellAddVideos")
            
        }
    }
    
    @IBOutlet weak var viewBottom: UIView!
    
    
    //MARK:- Properties
    
    var dataSource = [VideoModel]()
    let videoManager = PhotoManager()
    let fileManager = FileHadlingManager()
    var btnEdit = UIButton()
    var isEditClicked: Bool = false {
        didSet {
            if isEditClicked {
                /// Set title Done
                btnEdit.setTitle("Done", for: .normal)
                self.btnEdit.setImage(UIImage(named: ""), for: .normal)
                viewBottom.isHidden = false
                
            } else {
                /// Set Edit Icon
                btnEdit.setTitle("", for: .normal)
                self.btnEdit.setImage(UIImage(named: "editIcon"), for: .normal)
                viewBottom.isHidden = true
                getAllVideos()
            }
        }
    }
    
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setRightBarButton()
        viewBottom.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
        getAllVideos()
        isEditClicked = false
        
    }
    
    //MARK:- Actions
    @IBAction func btnAddVideos(_ sender: Any) {
        
        showAddVideosViewController()
    }
    @IBAction func btnSettings(_ sender: Any) {
        settingsViewController()
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let videos = getSelectedVideos()
        if videos.count > 0 {
        shareVideosFromApp(videos: videos)
        }
        else{
            showAlerttoShare()
        }
    }
    
    @IBAction func btnMoveOut(_ sender: Any) {
        let videos = getSelectedVideos()
        if videos.count > 0{
        let alert = UIAlertController(title: "Selected Videos will be moved out of the app?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            moveOutVideos(videos: getSelectedVideos())
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        }
        else{
            showAlerttoMoveOut()
        }
    }
    @IBAction func btnDelete(_ sender: Any) {
        let videos = getSelectedVideos()
        if videos.count > 0 {
        let alert = UIAlertController(title: "Are You Sure ?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            deleteSelectedVideos(videos: getSelectedVideos())
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        }
        else{
            showAlerttoDelete()
        }
        
    }
    
    @IBAction func btnEditFunc(_ sender: UIButton) {
        edit()
    }
    
    //MARK:- Setter
    func setRightBarButton() {
        
        self.btnEdit = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 28))
        btnEdit.setTitleColor(.white, for: .normal)
        btnEdit.setImage(UIImage(named: "editIcon"), for: .normal)
        btnEdit.addTarget(self, action: #selector(edit), for: .touchUpInside)
        self.setRightBar(Button: btnEdit)
    }
    
    //MARK:- Getter
    func getSelectedVideos() -> [VideoModel] {
        return dataSource.filter { (model) -> Bool in
            return model.isSelected ?? false
        }
    }
    
    func getAllVideos() {
        do {
           self.dataSource = try CoreDataManager.manager.getAllVideos()
           
        } catch {
        }
        videoCollectionView.reloadData()
    }
    
    
    //MARK:- Helping Methods
    @objc func edit(){
        if isEditClicked == false {
            isEditClicked = true
        }
        else if isEditClicked {
            isEditClicked = false
        }
    }
    
    func deleteSelectedVideos(videos: [VideoModel] ) {
        
        let paths = videos.compactMap { (model) -> String? in
            return model.path
        }
        fileManager.deleteDirectoriesBy(names: paths)
        CoreDataManager.manager.deleteVideos(videos: videos)
        getAllVideos()
    }
    
    func shareVideosFromApp(videos: [VideoModel]){
        guard let _videosUrl = fileManager.videoDirectoryURL else { return }
        var items = [Any]()
        for video in videos {
            guard let _video = try? Data(contentsOf: _videosUrl.appendingPathComponent(video.path ?? "")) else {
                continue
            }
            items.append(_video)
        }
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    
    func didSelectItemAtIndex(indexPath: IndexPath) {
        if isEditClicked {
            dataSource[indexPath.row].isSelected = !(dataSource[indexPath.row].isSelected ?? false)
            videoCollectionView.reloadItems(at: [indexPath])
        }
        else{
    //        showVideoPlayerViewController(model: dataSource[indexPath.row])
            setPlayer(video: dataSource[indexPath.row].path ?? "")
        }
    }
    
    func setPlayer(video : String) {
        guard let _photosUrl = fileManager.videoDirectoryURL else { return }
        let player = AVPlayer(url: _photosUrl.appendingPathComponent(video))
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true)
        
    }
    
    func setVideos (videos: [VideoModel]) {
        guard let _videosUrl = fileManager.photoDirectoryURL else { return }
        for video in videos {
            guard let _image = try? Data(contentsOf: _videosUrl.appendingPathComponent(video.path ?? "")) else {
                continue
            }
            let image = UIImage(data: _image)!
            CustomPhotoAlbum.sharedInstance.saveImage(image: image)
           // self.firstMoveOut = false

        }
        
    }
    
    func moveOutVideos(videos: [VideoModel]) {

        if videos.count > 0 {
            guard let _videosUrl = fileManager.videoDirectoryURL else { return }
            for video in videos {
                let _video = _videosUrl.appendingPathComponent(video.path ?? "")
                self.saveVideoToAlbum(_video) { (error) in
                    self.showDeleteVideosAlert()
                }
            }
        }
        else{
            showAlertWithTitle(message: "Please select Image to move out") {
                self.dismiss()
            }
        }
    }
    
    func requestAuthorization(completion: @escaping ()->Void) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization { (status) in
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else if PHPhotoLibrary.authorizationStatus() == .authorized{
            completion()
        }
    }
    
    func saveVideoToAlbum(_ outputURL: URL, _ completion: ((Error?) -> Void)?) {
        requestAuthorization {
            PHPhotoLibrary.shared().performChanges({
                let request = PHAssetCreationRequest.forAsset()
                request.addResource(with: .video, fileURL: outputURL, options: nil)
            }) { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("Saved successfully")
                    }
                    completion?(error)
                }
            }
        }
    }
    
    func showDeleteVideosAlert() {
        let alert = UIAlertController(title: "Moved successfully", message: "Do you want to delete moved videos?", preferredStyle: .alert)
        let actionContinue = UIAlertAction(title: "Continue", style: .default) {[unowned self] (action) in
            self.deleteSelectedVideos(videos: getSelectedVideos())
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive) {[unowned self] (action) in
            getAllVideos()
            self.dismiss()
        }
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        self.present(alert, animated: true) {
        }
    }
    
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
          print(error.localizedDescription)
          return nil
        }
    }
    
    //MARK:- Alerts
    
    func showAlerttoShare(){
        showAlertWithTitle(message: "Please select any video or videos to share") {
            self.dismiss()
        }
    }
    func showAlerttoDelete(){
        showAlertWithTitle(message: "Please select any video or videos to delete") {
            self.dismiss()
        }
    }
    func showAlerttoMoveOut(){
        showAlertWithTitle(message: "Please select any video or videos to moveout") {
            self.dismiss()
        }
    }
    
    
    
    //MARK:- Router

    func showAddVideosViewController() {
        let vc: AddVideosViewController = AddVideosViewController.initiateFrom(Storybaord: .media)
        push(toController: vc, animated: true)
    }
    
    func settingsViewController() {
        let vc: SettingsViewController = SettingsViewController.initiateFrom(Storybaord: .settings)
        push(toController: vc, animated: true)
    }
    

    
}

//MARK:- UICollectionViewDelegate
extension VideoViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        didSelectItemAtIndex(indexPath: indexPath)
    }
}

//MARK:- UICollectionViewDataSource
extension VideoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = dataSource[indexPath.row]
//        let cell = CollectionViewCellAddVideos.collectionView(collectionView, cellForItemAt: indexPath, data: data, fileManager: fileManager)
        let cell = CollectionViewCellAddVideos.collectionView(collectionView, cellForItemAt: indexPath, data: data, fileManager: fileManager)
        
        return cell

    }
    
}

//MARK: UICollectionViewDelegateFlowLayout
extension VideoViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 28, left: 0, bottom: 64, right: 0)
    }
}
