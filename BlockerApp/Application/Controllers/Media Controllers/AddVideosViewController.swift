//
//  AddVideosViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 29/06/2021.
//

import UIKit
import Photos
import PhotosUI

class AddVideosViewController: BaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewAddVideos: UICollectionView!{
        didSet {
            collectionViewAddVideos.delegate = self
            collectionViewAddVideos.dataSource = self
            collectionViewAddVideos.register(UINib(nibName: "CollectionViewCellAddVideos", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellAddVideos")
        }
    }
    @IBOutlet weak var btnAdd: UIButton!
    
    
    //MARK:- Properties
    var dataSource: [PhotoAssetModel] = []
    let videoManager = PhotoManager()
    let fileManager = FileHadlingManager()
    
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Video Library")
        setRightBarButton()
        getVideos()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    //MARK:- Setter
    
    func setRightBarButton() {
        
        self.btnAdd = self.setRightBarButton(WithTitle: "Add", selector: #selector(btnAdd(_:)))
        btnAdd.isEnabled = false
    }
    
    //MARK:- Getter
    
    func getVideos() {
        videoManager.photoAuthorization { (status) in
            switch status {
            case .authorized:
                self.videoManager.loadVideos{ [unowned self] (videos) in
                    self.dataSource = videos
                    DispatchQueue.main.async {
                        self.collectionViewAddVideos.reloadData()
                    }
                }
            case .limited:
                DispatchQueue.main.async {
                    self.showVideoPicker()
                }
            default:
                print("")
            }
        }
    }
    
    func getSelectedVideos() -> [PhotoAssetModel] {
        let filteredData = dataSource.compactMap { (model) -> PhotoAssetModel? in
            guard let _isSelected = model.isSelected, _isSelected == true else { return nil }
            return model
        }
        return filteredData
    }
    
    //MARK:- Actions
    
    @IBAction func btnAdd(_ sender: UIButton) {
        showSelectedVideosAlert()
    }
    
    //MARK:- Helping Methods
    
    func getThumbnail(){
        
        let asset = dataSource
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset[0].asset!, targetSize: CGSize(width: 138, height: 138), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        //  cell.imageViewVideo.image = thumbnail
        
    }
    
    
    
    func copySelectedVideos(videos: [PhotoAssetModel], completion: @escaping () -> Void) {
        let width = collectionViewAddVideos.frame.size.width / 3
        let height = width
        let group = DispatchGroup()
        var videosModel: [VideoModel] = []
        for video in videos {
            guard let _asset = video.asset else { continue }
            let innerGroup = DispatchGroup()
            group.enter()
            innerGroup.enter()
            innerGroup.enter()
            let model = VideoModel()
            videoManager.getUrlOf(asset: _asset, foType: .video) { [unowned self] (url) in
                guard let _url = url else {
                    innerGroup.leave()
                    return
                }
                self.fileManager.copyFile(file: _url, to: .video, byName: "\(video.assetId).\(_url.pathExtension)")
                model.videoId = UUID().uuidString
                model.path = "\(video.assetId).\(_url.pathExtension)"
                innerGroup.leave()
            }
            
            videoManager.loadMediaFrom(asset: video, preferredSize: CGSize(width: width, height: height)) { (image) in
                
                guard let _image = image else {
                    innerGroup.leave()
                    return
                }
                let imagName = "\(UUID().uuidString).png"
                _ = self.fileManager.save(image: _image, withName: imagName)
                model.thumbnail = imagName
                innerGroup.leave()
            }
            innerGroup.notify(queue: .main) {
                videosModel.append(model)
                group.leave()
            }
        }
        group.notify(queue: .main) {
            self.saveVideos(photos: videosModel)
            completion()
        }
    }
    
    func saveVideos(photos: [VideoModel]) {
        do {
            try CoreDataManager.manager.saveVideos(photos: photos)
        } catch {
            print("Videos not saved")
        }
    }
    
    func deleteSelectedVideos() {
        let videos = getSelectedVideos()
        copySelectedVideos(videos: videos) { [unowned self] in
            videoManager.deleteAssets(assets: videos) { [unowned self] (isSuccess) in
                DispatchQueue.main.async {
                    self.pop(animated: true)
                }
            }
        }
    }
    
    func showSelectedVideosAlert() {
        let video = getSelectedVideos()
        if video.count > 0 {
        let alert = UIAlertController(title: "", message: "Selected videos will be delete from gallery. Are you sure?", preferredStyle: .alert)
        let actionContinue = UIAlertAction(title: "Continue", style: .default) { [unowned self] (action) in
            self.deleteSelectedVideos()
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive) { (action) in }
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        self.present(alert, animated: true) {
        }
    }
    }
    
    func showVideoPicker() {
        let photoLibrary = PHPhotoLibrary.shared()
        if #available(iOS 14, *) {
            let configuration = PHPickerConfiguration(photoLibrary: photoLibrary)
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            present(picker, animated: true)
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
}

//MARK:- UICollectionViewDelegate
extension AddVideosViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dataSource[indexPath.row].isSelected = !(dataSource[indexPath.row].isSelected ?? true)
        let selectedVideos = getSelectedVideos()
        btnAdd.isEnabled = (selectedVideos.count > 0) ? true : false
        
        collectionView.reloadItems(at: [indexPath])
    }
}

//MARK:- UICollectionViewDataSource
extension AddVideosViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = dataSource[indexPath.row]
        let cell = CollectionViewCellAddVideos.collectionView(collectionView, cellForItemAt: indexPath, data: data, photoManager: videoManager)
        
        return cell
    }
    
}

//MARK:- UICollectionViewDelegateFlowLayout
extension AddVideosViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension AddVideosViewController: PHPickerViewControllerDelegate {
    @available(iOS 14, *)
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)
        
        let identifiers = results.compactMap(\.assetIdentifier)
        videoManager.loadPhotosBy(identifiers: identifiers) { [unowned self] (photos) in
            self.dataSource = photos
            DispatchQueue.main.async {
                self.collectionViewAddVideos.reloadData()
            }
        }
    }
}

    


