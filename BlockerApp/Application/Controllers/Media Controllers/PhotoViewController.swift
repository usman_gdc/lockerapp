//
//  PhotoViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 13/06/2021.
//


import UIKit
import PhotosUI


class PhotoViewController: BaseViewController {
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    //MARK:- IBOutlets
    @IBOutlet weak var imageCollectionView: UICollectionView! {
        didSet {
            imageCollectionView.dataSource = self
            imageCollectionView.delegate = self
            imageCollectionView.register(UINib(nibName: "CollectionViewCellAddPhoto", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellAddPhoto")
        }
    }
    @IBOutlet weak var viewBottom: UIView!

    //MARK:- Properties
    
    var dataSource = [PhotoModel]()
    let photoManager = PhotoManager()
    let fileManager = FileHadlingManager()
    var btnEdit = UIButton()
    var firstTime: Bool = true
    var photos = [PhotoModel]()
   // var firstMoveOut: Bool = CustomPhotoAlbum.firsttime ?? true
    var isEditClicked: Bool = false {
        didSet {
            if isEditClicked {
                /// Set title Done
                btnEdit.setTitle("Done", for: .normal)
                self.btnEdit.setImage(UIImage(named: ""), for: .normal)
                viewBottom.isHidden = false
                
            } else {
                /// Set Edit Icon
                btnEdit.setTitle("", for: .normal)
                self.btnEdit.setImage(UIImage(named: "editIcon"), for: .normal)
                viewBottom.isHidden = true
                getAllPhotos()
            }
            
        }
    }    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setRightBarButton()
        viewBottom.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
        getAllPhotos()
        isEditClicked = false
        
    }
    
    //MARK:- Actions
    @IBAction func btnAddPhotos(_ sender: Any) {
        if isEditClicked && firstTime {
            showAlertWithTitle(message: "Loading photos will take a few moments") {
                self.dismiss()
                self.firstTime = false
                self.showAddPhotosViewController()
            }
            
        }
        showAddPhotosViewController()
    }
    
    @IBAction func btnSettings(_ sender: Any) {
        settingsViewController()
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let photos = getSelectedPhotos()
        if photos.count > 0{
       shareImageFromApp(photos: photos)
    }
    else {
    showAlerttoShare()
    }
    }
    
    
    @IBAction func btnMoveOut(_ sender: Any) {
        let photos = getSelectedPhotos()
        if photos.count > 0{
        let alert = UIAlertController(title: "Selected Photos will be moved out of the app?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            moveOutImages(photos: getSelectedPhotos())
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        }
        else{
            showAlerttoMoveOut()
        }
    }
    @IBAction func btnDelete(_ sender: Any) {
        let photos = getSelectedPhotos()
        if photos.count > 0 {
        let alert = UIAlertController(title: "Are You Sure ?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            deleteSelectedPhoto(photos: getSelectedPhotos())
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        }
        else{
            showAlerttoDelete()
        }
        
    }
    
    
    @IBAction func btnEditFunc(_ sender: UIButton) {
        edit()
    }
    
    //MARK:- Setter
    func setRightBarButton() {
        self.btnEdit = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 28))
        btnEdit.setTitleColor(.white, for: .normal)
        btnEdit.setImage(UIImage(named: "editIcon"), for: .normal)
        btnEdit.addTarget(self, action: #selector(edit), for: .touchUpInside)
        self.setRightBar(Button: btnEdit)
        
    }
    
    //MARK:- Getter
    func getSelectedPhotos() -> [PhotoModel] {
        return dataSource.filter { (model) -> Bool in
            return model.isSelected ?? false
        }
    }
    
    func getAllPhotos() {
        do {
            self.dataSource = try CoreDataManager.manager.getAllPhotos()
        } catch {
        }
        imageCollectionView.reloadData()
    }
    
    
    //MARK:- Helping Methods
    @objc func edit(){
        if isEditClicked == false {
            isEditClicked = true
        }
        else if isEditClicked {
            isEditClicked = false
        }
        
    }
    
    func deleteSelectedPhoto(photos: [PhotoModel] ) {
        
        let paths = photos.compactMap { (model) -> String? in
            return model.path
        }
        fileManager.deleteDirectoriesBy(names: paths)
        CoreDataManager.manager.deletePhotos(photos: photos)
        getAllPhotos()
        
    }
    
    func shareImageFromApp(photos: [PhotoModel]){
        guard let _photosUrl = fileManager.photoDirectoryURL else { return }
        var items = [Any]()
        if photos.count > 0{
        for photo in photos {
            guard let _image = try? Data(contentsOf: _photosUrl.appendingPathComponent(photo.path ?? "")) else {
                continue
            }
            items.append(_image)
        }
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        }
    }
    
    func didSelectItemAtIndex(indexPath: IndexPath) {
        if isEditClicked {
            dataSource[indexPath.row].isSelected = !(dataSource[indexPath.row].isSelected ?? false)
            imageCollectionView.reloadItems(at: [indexPath])
        }
        else{
            photoPreviewViewController(model: dataSource[indexPath.row])
        }
    }
    
//    func setPhotos (photos: [PhotoModel]) {
//        guard let _photosUrl = fileManager.photoDirectoryURL else { return }
//        for photo in photos {
//            guard let _image = try? Data(contentsOf: _photosUrl.appendingPathComponent(photo.path ?? "")) else {
//                continue
//            }
//            let image = UIImage(data: _image)!
//            CustomPhotoAlbum.sharedInstance.saveImage(image: image)
//            self.firstMoveOut = false
//        }
//    }
    
    func moveOutImages(photos: [PhotoModel]) {

        if photos.count > 0 {
            
            guard let _photoUrl = fileManager.photoDirectoryURL else { return }
            for photo in photos {
                let _photo = _photoUrl.appendingPathComponent(photo.path ?? "")
                self.savePhotoToAlbum(_photo) { (error) in
                    self.showDeletePhotosAlert()                }
            }
        }
        else{
            showAlertWithTitle(message: "Please select Image to move out") {
                self.dismiss()
            }
        }
    }
    
    func requestAuthorization(completion: @escaping ()->Void) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization { (status) in
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else if PHPhotoLibrary.authorizationStatus() == .authorized{
            completion()
        }
    }
    
    func savePhotoToAlbum(_ outputURL: URL, _ completion: ((Error?) -> Void)?) {
        requestAuthorization {
            PHPhotoLibrary.shared().performChanges({
                let request = PHAssetCreationRequest.forAsset()
                request.addResource(with: .photo, fileURL: outputURL, options: nil)
            }) { (result, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("Saved successfully")
                    }
                    completion?(error)
                }
            }
        }
    }
    
//    PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
//        let request = PHAssetChangeRequest(forAsset: asset)
//        request.creationDate = dateTime!
//    }, completionHandler: { (success: Bool, error: NSError?) -> Void in
//        dispatch_async(dispatch_get_main_queue()) {
//            //done
//        }
//    })
    
    
    
    func showDeletePhotosAlert() {
        let alert = UIAlertController(title: "Moved successfully", message: "Do you want to delete moved photos?", preferredStyle: .alert)
        let actionContinue = UIAlertAction(title: "Continue", style: .default) {[unowned self] (action) in
            self.deleteSelectedPhoto(photos: getSelectedPhotos())
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive) {[unowned self] (action) in
            getAllPhotos()
            self.dismiss()
        }
        
        alert.addAction(actionContinue)
        alert.addAction(actionCancel)
        self.present(alert, animated: true) {
        }
    }
    //MARK: Alerts
    
    func showAlerttoShare(){
        showAlertWithTitle(message: "Please select any photo or photos to share") {
            self.dismiss()
        }
    }
    func showAlerttoDelete(){
        showAlertWithTitle(message: "Please select any photo or photos to delete") {
            self.dismiss()
        }
    }
    func showAlerttoMoveOut(){
        showAlertWithTitle(message: "Please select any photo or photos to moveout") {
            self.dismiss()
        }
    }
    
    //MARK: Router

    func showAddPhotosViewController() {
        let vc: AddPhotosViewController = AddPhotosViewController.initiateFrom(Storybaord: .media)
        push(toController: vc, animated: true)
    }
    
    func settingsViewController() {
        let vc: SettingsViewController = SettingsViewController.initiateFrom(Storybaord: .settings)
        push(toController: vc, animated: true)
    }
    
    func photoPreviewViewController(model: PhotoModel) {
        let vc: PhotoViewerViewController = PhotoViewerViewController.initiateFrom(Storybaord: .media)
        vc.photoModel = model
           push(toController: vc, animated: true)
    }
    
    
}

//MARK: UICollectionViewDelegate
extension PhotoViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectItemAtIndex(indexPath: indexPath)
    }
}

//MARK: UICollectionViewDataSource
extension PhotoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = dataSource[indexPath.row]
        let cell = CollectionViewCellAddPhoto.collectionView(collectionView, cellForItemAt: indexPath, data: data, fileManager: fileManager)
        return cell

    }
    
}

//MARK: UICollectionViewDelegateFlowLayout
extension PhotoViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 28, left: 0, bottom: 64, right: 0)
    }
}






