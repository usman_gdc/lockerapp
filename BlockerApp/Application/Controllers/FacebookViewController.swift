//
//  FacebookViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 03/06/2021.
//

import UIKit
import WebKit

class FacebookViewController: UIViewController, WKUIDelegate {
    
    
    @IBOutlet weak var webViewFb: WKWebView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        let webConfiguration = WKWebViewConfiguration()
       // webViewFb = WKWebView(frame: .zero, configuration: webConfiguration)
        webViewFb.uiDelegate = self

        let myURL = URL(string:"https://www.google.com")
                let myRequest = URLRequest(url: myURL!)
                webViewFb.load(myRequest)
    }
    
    @IBAction func historyBtn(_ sender: Any) {
        
        for page in webViewFb.backForwardList.backList {
            print("User visited \(page.url.absoluteString)")
        }
        
    }
    
    

}
