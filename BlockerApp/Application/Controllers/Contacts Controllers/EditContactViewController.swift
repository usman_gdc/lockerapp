//
//  EditContactViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 15/06/2021.
//

import UIKit

class EditContactViewController: BaseViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    
    //MARK:- Properties
    
   var contactDetails = ContactModel()
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.text = contactDetails.contactName
        txtNumber.text = contactDetails.contactNumber

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        setBackButton()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(updateContact))
        return
    }
    
    //MARK:- Actions
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
 
    func editContact(contactId: String , contactName: String, contactNumber: String){
        do {
            try CoreDataManager.manager.updateContact(contactId: contactId, contactName: contactName, contactNumber: contactNumber)
            print("Note updated Succcessfully")
        }catch{
            print("Error updating note")
        }
    }
    
    
    @objc func updateContact (){
        
        if txtName.text!.trim.count > 0{
            
             if txtNumber.text!.trim.count > 0{
                editContact(contactId: contactDetails.contactId!, contactName: txtName.text!, contactNumber: txtNumber.text!)
                
                showAlertWithTitle(message: "Contact saved Successfully") {
                    self.goToContactViewController()
                    
                }

            }
            
            else
            {
                showAlertWithTitle(title: "Number is not entered", message: "Please Enter Number", cancelTitle: "Cancel", otherTitle: "OK") {
                    self.txtNumber.becomeFirstResponder()
                } otherButton: {
                    self.txtNumber.becomeFirstResponder()
                    
                }

                
            }
        }
        
        else{
            
            showAlertWithTitle(title: "Name is Empty", message: "Please Enter Name", cancelTitle: "Cancel", otherTitle: "OK") {
                self.txtName.becomeFirstResponder()
            } otherButton: {
                self.txtName.becomeFirstResponder()
            }

        }
        
    }
    
    
    //MARK:- Router
    
    func goToContactViewController() {
       // self.navigationController?.popViewController(animated: true)
        self.pop()
    }

}

//MARK:- Extensions
