//
//  NewContactViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 15/06/2021.
//

import UIKit
import CoreData

class NewContactViewController: BaseViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    
    //MARK:- Properties
    
   // var data: CoreDataManager
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        setBackButton()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(addTapped))
        return
    }
    
    //MARK:- Actions
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func saveContacts() {
        
        var model = ContactModel()
        model.contactId = UUID().uuidString
        model.contactName = txtName.text
        model.contactNumber = txtNumber.text
        do {
             try CoreDataManager.manager.saveContact(contact: model)
            print("Contact Saved")
        } catch {
            print("Exception occur")
        }
        
    }
    
    
    @objc func addTapped (){
        if txtName.text!.trim.count > 0{
             if txtNumber.text!.trim.count > 0{
                saveContacts()
                showAlertWithTitle(message: "Contact saved Successfully") {
                    self.goToContactViewController()
                }
            }
            else
            {
                showAlertWithTitle(title: "Number is not entered", message: "Please Enter Number", cancelTitle: "Cancel", otherTitle: "OK") {
                    self.txtNumber.becomeFirstResponder()
                } otherButton: {
                    self.txtNumber.becomeFirstResponder()
                }
            }
        }
        
        else{
            
            showAlertWithTitle(title: "Name is Empty", message: "Please Enter Name", cancelTitle: "Cancel", otherTitle: "OK") {
                self.txtName.becomeFirstResponder()
            } otherButton: {
                self.txtName.becomeFirstResponder()
            }
        }
    }
    
    
    //MARK:- Router
    
    func goToContactViewController() {
       // self.navigationController?.popViewController(animated: true)
        self.pop()
    }

}

//MARK:- Extensions
