//
//  ContactViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 13/06/2021.
//

import UIKit
import ContactsUI


class ContactViewController: BaseViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var contactTableView: UITableView!
    
    @IBOutlet weak var searchBarContact: UISearchBar!
    
    //MARK:- Properties
    
    var contactArray = [ContactModel]()
    var searchedContacts = [ContactModel]()
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactTableView.dataSource = self
        contactTableView.delegate = self
        searchBarContact.delegate = self
        
        contactTableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
        fetchContacts()
        
    }
    
    //MARK:- Actions
    
    @IBAction func btnNewContact(_ sender: Any) {
        newContactViewController()
    }
    
    @IBAction func btnImportFromContacts(_ sender: Any) {
        showMutipleContactPickerViewController()
    }
    
    @IBAction func btnAddApplication(_ sender: UIButton) {
        
//        let appData = appsArray[sender.tag]
//        if appData.isExisted ?? false {
//
//        }
    }
    
    
    
    //MARK:- Setter
    
    func setImportedContacts(contacts: [ContactModel]) {
        do {
            try CoreDataManager.manager.importContatcs(contacts: contacts)
        } catch {
        }
        fetchContacts()
    }
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func importFromContacts() -> [ContactModel]{
        let contactStore = CNContactStore()
        var contactsData = [ContactModel]()
        let key = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try? contactStore.enumerateContacts(with: request, usingBlock: { (contact, stoppingPointer) in
            let contactId = UUID().uuidString
            let givenName = contact.givenName
            let familyName = contact.familyName
            let phoneNumber: [String] = contact.phoneNumbers.map{ $0.value.stringValue }
            // let phoneNumber = contact.phoneNumbers.first?.value.stringValue ?? ""
            contactsData.append(ContactModel(contactId: contactId, contactName: "\(givenName) \(familyName)", contactNumber: phoneNumber.first))
        })
        return contactsData
    }
    
    
    func fetchContacts() {
        
        do {
            contactArray = try CoreDataManager.manager.getAllContacts()
            
            searchedContacts = contactArray
        } catch {
        }
        contactTableView.reloadData()
    }
    
    func deleteContact(contactId: String ) {
        
        do {
             try CoreDataManager.manager.deleteContactBy(contactId: contactId)
        } catch {
        }
        fetchContacts()
    }
    
    
    
    //MARK:- Router
    
    func newContactViewController() {
        let vc: NewContactViewController = NewContactViewController.initiateFrom(Storybaord: .contacts)
        push(toController: vc, animated: true)
    }
    
    func editContactViewController(model: ContactModel) {
        let vc: EditContactViewController = EditContactViewController.initiateFrom(Storybaord: .contacts)
        vc.contactDetails = model
        push(toController: vc, animated: true)
    }

    
    func showMutipleContactPickerViewController() {
        let controller: MultiContactPickerViewController = MultiContactPickerViewController.initiateFrom(Storybaord: .contacts)
        controller.delegate = self
        let navController = UINavigationController(rootViewController: controller)
        self.present(navController, animated: true) {
            
        }
        //        let vc: MultiContactPickerViewController = MultiContactPickerViewController
        //          push(toController: vc, animated: true)
    }
    
    
    
}

//MARK:- UITableViewDataSource
extension ContactViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ContactTableViewCell = self.contactTableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as! ContactTableViewCell
        
//        cell.lblContactName.text = contactArray[indexPath.row].contactName
//        cell.lblContactNumber.text = contactArray[indexPath.row].contactNumber
        
        cell.lblContactName.text = searchedContacts[indexPath.row].contactName
        cell.lblContactNumber.text = searchedContacts[indexPath.row].contactNumber
        cell.copyContact = {
            UIPasteboard.general.string = cell.lblContactNumber.text!
            self.showAlertWithTitle(message: "Contact Copied Successfully") {
                return
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{
//            tableView.beginUpdates()
            deleteContact(contactId: contactArray[indexPath.row].contactId ?? "")
//            tableView.endUpdates()
        }
        
    }
    
}

//MARK:- UITableViewDelegate
extension ContactViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        editContactViewController(model: contactArray[indexPath.row])
    }
}

//MARK:- MultiContactPickerProtocol
extension ContactViewController: MultiContactPickerProtocol {
    
    func selectedContacts(cNContact : [CNContact]) {
        var contacts: [ContactModel] = []
        for contact in cNContact {
            var contactModel = ContactModel()
            contactModel.contactId = UUID().uuidString
            contactModel.contactName = contact.givenName + " " + contact.middleName + " " + contact.familyName
            
            if let _first = contact.phoneNumbers.first {
                contactModel.contactNumber = _first.value.stringValue
            }
            contacts.append(contactModel)
        }
        self.setImportedContacts(contacts: contacts)
        
    }
}


extension ContactViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchedContacts = contactArray
            
        }else {
            searchedContacts = contactArray.filter({($0.contactName)!.lowercased().contains(searchText.lowercased())})
        }
        self.contactTableView.reloadData()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
