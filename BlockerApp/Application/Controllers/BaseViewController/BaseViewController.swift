//
//  BaseViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 10/06/2021.
//
import GoogleMobileAds
import UIKit
import StoreKit
import LocalAuthentication
import MBProgressHUD

class BaseViewController: UIViewController {
    
    var rewardedAd: GADRewardedAd?
    var gotReward = 0
    var interstitial: GADInterstitialAd?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  overrideUserInterfaceStyle = .light
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addApplicationBecomeActiveNotificationObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeApplicationBecomeActiveNotificationObserver()
    }
    
    func setTitle(title: String?) {
        self.title = title
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    func setBackButton() {
        self.navigationController?.setNavigation(tintColor: .white, andBackgroundColor: UIColor.Color.lightPurple)
    }
    func setNavigationBarSystemGray6() {
        self.navigationController?.setNavigation(tintColor: .systemBlue, andBackgroundColor: .systemGray6)
    }
    
    func push(toController controller: UIViewController, animated: Bool = true) {
        self.navigationController?.pushViewController(controller, animated: animated)
    }
    
    func pop(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func present(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func dismiss(animated: Bool = true) {
        self.dismiss(animated: animated) {
        }
    }
    
    func presentFullScreenViewControllerFromMain(vc:ViewController) {
        
        let controller = ViewController.initiateFrom(Storybaord: .main)
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
    }
    
    
    func presentEnterPassCodeViewController() {
        
        let controller = EnterPasswordViewController.initiateFrom(Storybaord: .main)
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
    }
    
    func presentPassCodeViewController(isLockSuccessful: ((_ isSuccess: Bool) -> Void)? = nil) {
        let controller = PasscodeViewController.initiateFrom(Storybaord: .main)
        controller.isLockSuccessful = { (isSuccess) in
            guard let _isLockSuccessful = isLockSuccessful else { return }
            _isLockSuccessful(isSuccess)
        }
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
        
    }
    
    func presentPatternViewController(isLockSuccessful: ((_ isSuccess: Bool) -> Void)? = nil) {
        
        let controller = PatternViewController.initiateFrom(Storybaord: .main)
        controller.isLockSuccessful = { (isSuccess) in
            guard let _isLockSuccessful = isLockSuccessful else { return }
            _isLockSuccessful(isSuccess)
        }
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
        
    }
    
    func presentFaceIdViewController(isLockSuccessful: ((_ isSuccess: Bool) -> Void)? = nil) {
        
        let controller = FaceIDViewController.initiateFrom(Storybaord: .main)
        controller.isLockSuccessful = {(isSuccess) in
            guard let _isLockSuccessful = isLockSuccessful else { return }
            _isLockSuccessful(isSuccess)
        }
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
        
    }
    
    
    //MARK:- FaceID
    
    func showFaceIdAuthentication(completion: @escaping(Bool) -> Void) {
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
            let reason = "Please Identify"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] success, error in
                
                guard success else {
                    DispatchQueue.main.async {
                        guard success, error == nil
                        else
                        {
                            let alert = UIAlertController(title: "Failed to Authenticate", message: "Please try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                            self?.present(alert,animated: true)
                            return
                        }
                    }
                    return
                }
                completion(success)
            }
        }
        
        else{
            let alert = UIAlertController(title: "Unavailable", message: "Your device cannot use this feature", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            present(alert,animated: true)
            
        }
        
    }
}

extension BaseViewController {
    
    func checkAppPassCode() {
        //UserDefaults.standard.appPassCode != nil &&
//        if (UserDefaults.standard.appPassCode == nil && UserDefaults.standard.firstSignUp == false) {
//        }
        if checkTopViewController() {
            return
        }
        
        guard let _passCode = UserDefaults.standard.appPassCode, let _passCodeType = _passCode.passCodeType else {
            self.presentEnterPassCodeController()
            return
        }
        self.presentPassCodeControllerWith(Type: _passCodeType)
    }
    
    func checkTopViewController() -> Bool {
        guard let _topController = UIApplication.topViewController() else {
            return false
        }
        if _topController.isKind(of: PasscodeViewController.self) {
            return true
        }
        if _topController.isKind(of: ConfirmPasswordViewController.self) {
            return true
        }
        if _topController.isKind(of: EnterPasswordViewController.self){
            return true
        }
        if _topController.isKind(of: PatternViewController.self){
            return true
        }
        if _topController.isKind(of: NewPatternViewController.self){
            return true
        }
        if _topController.isKind(of: FaceIDViewController.self){
            return true
        }
        return false
    }
    func presentEnterPassCodeController() {
        guard let _topController = UIApplication.topViewController(), let _baseViewController = _topController as? BaseViewController else {
            return
        }
        _baseViewController.presentEnterPassCodeViewController()
    }
    
    func presentPassCodeControllerWith(Type type: AppPassCodeModel.PassCodeType) {
        guard let _topController = UIApplication.topViewController(), let _baseViewController = _topController as? BaseViewController else {
            return
        }
        switch type {
        case .passCode:
            _baseViewController.presentPassCodeViewController()
            return
        case .pattern:
            _baseViewController.presentPatternViewController()
            return
        case .faceId:
            _baseViewController.presentFaceIdViewController()
            return
        }
    }
    
}

extension BaseViewController {
    
    @objc func applicationDidBecomeActive() {
        checkAppPassCode()
    }
    
    func addApplicationBecomeActiveNotificationObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification, // UIApplication.didBecomeActiveNotification for swift 4.2+
                                               object: nil)
    }
    
    func removeApplicationBecomeActiveNotificationObserver() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}

// // MARK:- BannerAd
//extension BaseViewController {
//    
//    func loadBannerAd(viewBannerAd : GADBannerView){
//        // Working
//        viewBannerAd.adUnitID = "ca-app-pub-3940256099942544/2934735716"
//        viewBannerAd.rootViewController = self
//        viewBannerAd.load(GADRequest())
//        
//        
//    }
//    
//}

// MARK:- RewardedAd

extension BaseViewController {
    func loadRewardedAd(adCase : Int){
        self.showHud("Loading...")
        GADRewardedAd.load(
            withAdUnitID: "ca-app-pub-3940256099942544/1712485313", request: GADRequest()
        ) { [weak self] (ad, error) in
            
            guard let `self` = self else { return }
            self.hideHUD()
            
            if let error = error {
                print("Rewarded ad failed to load with error: \(error.localizedDescription)")
                return
            }
            print("Loading Succeeded")
            self.rewardedAd = ad
            self.rewardedAd?.fullScreenContentDelegate = self
            
            self.rewardedAd?.present(fromRootViewController: self, userDidEarnRewardHandler: {
                let reward =  self.rewardedAd?.adReward
                self.getReward(NSInteger(truncating: reward?.amount ?? 0))
                
            })
        }
    }
    
    fileprivate func getReward(_ reward: NSInteger) {
        gotReward = reward
        print("Reward: \(self.gotReward)")
    }
}

// MARK:- InterstitialAd

extension BaseViewController  {
    func loadInterstitialAd(adCase : Int)  {
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID:"ca-app-pub-3940256099942544/4411468910",
                               request: request,
                               completionHandler: { [self] ad, error in
                                if let error = error {
                                    print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                    return
                                }
                                interstitial = ad
                                interstitial?.fullScreenContentDelegate = self
                                interstitial?.present(fromRootViewController: self)
                                switch adCase {
                                case 3 :
                                    appViewController()
                                case 4 :
                                    foldersViewController()
                                case 5 :
                                    noteViewController()
                                case 6 :
                                    contactViewController()
                                default:
                                    return
                                }
                               })
        
    }
    
    func appViewController() {
        let vc: AppViewController = AppViewController.initiateFrom(Storybaord: .browser)
        push(toController: vc, animated: true)
    }
    
    func foldersViewController() {
        let vc: FoldersViewController = FoldersViewController.initiateFrom(Storybaord: .browser)
        let fileManager: FileHadlingManager = FileHadlingManager()
        if let _root = fileManager.fileDirectoryURL {
            vc.rootDirectory = RootDirectory(name: FileHadlingManager.fileDirectoryName, path: _root.lastPathComponent)
        }
        push(toController: vc, animated: true)
    }
    
    func noteViewController() {
        let vc: NoteViewController = NoteViewController.initiateFrom(Storybaord: .browser)
        push(toController: vc, animated: true)
    }
    
    func contactViewController() {
        let vc: ContactViewController = ContactViewController.initiateFrom(Storybaord: .contacts)
        push(toController: vc, animated: true)
    }
    
}


extension BaseViewController : GADFullScreenContentDelegate {
    
    
}

// MARK:- WriteReview
//extension BaseViewController {
//
//    func writeReview(){
//        // 1. https://itunes.apple.com/app/id958625272
//        // https://apps.apple.com/us/app/charging-play-animation-show/id1577340989
//        guard let productURL = URL(string: "https://apps.apple.com/us/app/charging-play-animation-show/id1577340989") else { return }
//        var components = URLComponents(url: productURL , resolvingAgainstBaseURL: false)
//        // 2.
//        components?.queryItems = [
//          URLQueryItem(name: "action", value: "write-review")
//        ]
//        // 3.
//        guard let writeReviewURL = components?.url else {
//          return
//        }
//        // 4.
//        UIApplication.shared.open(writeReviewURL)
//    }
//}


extension UITableViewController {
    func showHudForTable(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
        hud.layer.zPosition = 2
        self.tableView.layer.zPosition = 1
    }
}

extension UIViewController {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
    }

    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
