//
//  FileBrowserViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 25/07/2021.
//

import UIKit
import WebKit

class FileBrowserViewController: BaseViewController {

    @IBOutlet fileprivate weak var webView: WKWebView! {
        didSet {
            webView.navigationDelegate = self
            webView.uiDelegate = self
        }
    }
    
    //MARK:- Properties
    var url: String = ""
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
        loadWebpage()
    }
    

    //MARK:- Actions
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func loadWebpage(){
        print(url)
        let myURL = URL(fileURLWithPath: url)
        let request = URLRequest(url: myURL)
        webView.load(request)
//        webView.loadFileURL(myURL, allowingReadAccessTo: myURL.deletingLastPathComponent())
    }
}

extension FileBrowserViewController: WKUIDelegate, WKNavigationDelegate {

    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {

        webView.load(navigationAction.request)
        return nil;
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let serverTrust = challenge.protectionSpace.serverTrust  else {
            completionHandler(.useCredential, nil)
            return
        }
        let credential = URLCredential(trust: serverTrust)
        completionHandler(.useCredential, credential)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
       decisionHandler(.allow)
    }

}

//extension FileBrowserViewController: WKNavigationDelegate {
//
//}
