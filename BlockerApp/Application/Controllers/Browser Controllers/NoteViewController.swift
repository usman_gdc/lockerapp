//
//  NoteViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 13/06/2021.
//

import UIKit

class NoteViewController: BaseViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var noteCollectionView: UICollectionView!
    
    //MARK:- Properties
 
    var notesArray = [NotesModel]()
    
   
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        noteCollectionView.dataSource = self
        noteCollectionView.delegate = self
        
        noteCollectionView.register(UINib(nibName: "NoteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoteCollectionViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        setBackButton()
        
        fetchNotes()
        
        noteCollectionView.reloadData()
    }
    
    //MARK:- Actions
    
    @IBAction func btnNewNote(_ sender: Any) {
        
        newNoteViewController()
        
    }
    
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func fetchNotes(){
    do {
        notesArray = try CoreDataManager.manager.getAllNotes()
        
    } catch {
        print("Exception occur")
    }
}
    
    //MARK:- Router
    
    func newNoteViewController() {
        let vc: NewNoteViewController = NewNoteViewController.initiateFrom(Storybaord: .browser)
           push(toController: vc, animated: true)
    }
    
    func editNoteViewController(model: NotesModel) {
        let vc: EditNoteViewController = EditNoteViewController.initiateFrom(Storybaord: .browser)
        vc.noteDetail = model
           push(toController: vc, animated: true)
    }
    

}

//MARK:- Extensions NoteViewController

extension NoteViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        editNoteViewController(model: notesArray[indexPath.row])
    }
    
}


extension NoteViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = noteCollectionView.dequeueReusableCell(withReuseIdentifier: "NoteCollectionViewCell", for: indexPath) as! NoteCollectionViewCell
        cell.lblNoteTitle.text = notesArray[indexPath.row].title
        return cell
    }
}

extension NoteViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       let width = view.frame.size.width - 75
        return CGSize(width: width/3, height: width/3)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //vertical padding
        return 15    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        //horizontal padding
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 20, left: 20, bottom: 0, right: 20)
    }
}
