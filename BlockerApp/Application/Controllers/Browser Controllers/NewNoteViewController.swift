//
//  NewNoteViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 15/06/2021.
//

import UIKit




class NewNoteViewController: BaseViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var txtTitle: UITextField!
    
    
    @IBOutlet weak var txtNote: RSKPlaceholderTextView!
    
    
    
    //MARK:- Properties
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        txtNote.placeholder = "Note"
        txtNote.placeholderColor = UIColor.Color.placeHolderTextColor
        self.txtTitle.becomeFirstResponder()
        setBackButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveNewNote))
    }
    
    //MARK:- Actions
    
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func setValues (){
        
        var model = NotesModel()
        model.noteId = UUID().uuidString
        model.title = txtTitle.text
        model.note = txtNote.text
        do {
             try CoreDataManager.manager.saveNote(note: model)
        } catch {
            print("Exception occur")
        }
    }

    @objc func saveNewNote (){
        if txtTitle.text!.trim.count > 0{
            if txtNote.text!.trim.count > 0{
                setValues()
                
                showAlertWithTitle(message: "Note is saved Successfully") {
                    self.goToNoteViewController()
                }
            }
            
            else
            {
                showAlertWithTitle(title: "Note's Description is Empty", message: "Please Enter Description", cancelTitle: "Cancel", otherTitle: "OK") {
                    self.txtNote.becomeFirstResponder()
                } otherButton: {
                    
                }
            }
        }
        
        else{
            
            showAlertWithTitle(title: "Note's Title is Empty", message: "Please Enter Title", cancelTitle: "Cancel", otherTitle: "OK") {
                self.txtTitle.becomeFirstResponder()
            } otherButton: {
                
            }

        }
        
    }
    
    
    
    
    //MARK:- Router
    
    func goToNoteViewController() {
       // self.navigationController?.popViewController(animated: true)
        self.pop()
    }
    
}

//MARK:- Extensions
