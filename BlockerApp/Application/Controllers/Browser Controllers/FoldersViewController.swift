//
//  FoldersViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 01/07/2021.
//

import UIKit
import MobileCoreServices
import QuickLook

//MARK:- IBOutlets
typealias RootDirectory = (name: String, path: String)

class FoldersViewController: BaseViewController {
    
    //MARK:- ENUM
    enum RightBarButtonType {
        case edit
        case cancel
        case none
    }
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnAddFolder: UIButton!
    @IBOutlet weak var btnMoveToFolder: UIButton!
    @IBOutlet weak var btnRename: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var foldersCollectionView: UICollectionView!{
        didSet {
            foldersCollectionView.dataSource = self
            foldersCollectionView.delegate = self
            foldersCollectionView.register(UINib(nibName: "FoldersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FoldersCollectionViewCell")
            
        }
    }
    
    //MARK:- Properties
    var alertAddFolder: UIAlertController?
    var alertRename: UIAlertController?
    var alertDeleteFolder: UIAlertController?
    let fileManager: FileHadlingManager = FileHadlingManager()
    var dataSource: [FileModel] = []
    var txtFolderName: UITextField?
    var txtRename: UITextField?
    var btnSelectedAll: UIButton?
    lazy var previewItem = NSURL()
    lazy var previewUrl: URL? = nil
    var rootDirectory: RootDirectory = (name: "", path: "")
    
    var hideAddButton: Bool = false {
        didSet {
            btnAddFolder.isHidden = hideAddButton
        }
    }
    
    var hideBottomView: Bool = true {
        didSet {
            viewBottom.isHidden = hideBottomView
        }
    }
    
    var hideMoveToFolder: Bool = true {
        didSet {
            btnMoveToFolder.isHidden = hideMoveToFolder
        }
    }
    
    var hideRename: Bool = true {
        didSet {
            btnRename.isHidden = hideRename
        }
    }
    
    var hideDelete: Bool = true {
        didSet {
            btnDelete.isHidden = hideDelete
        }
    }
    
    var isSelectedAll: RightBarButtonType = .edit {
        didSet {
            
            switch isSelectedAll {
            case .edit:
                btnSelectedAll?.setTitle("Cancel", for: .normal)
                self.btnSelectedAll?.setImage(nil, for: .normal)
                viewBottom.isHidden = false
                break
            case .cancel, .none:
                btnSelectedAll?.setTitle("", for: .normal)
                self.btnSelectedAll?.setImage(UIImage(named: "editIcon"), for: .normal)
                viewBottom.isHidden = true
                break
            }
        }
    }
    
    var isAnyFolderSelected: Bool {
        get {
            for (_, object) in self.dataSource.enumerated() {
                if object.isSelected ?? false && object.isDirectory ?? .folder == .folder {
                    return true
                }
            }
            return false
        }
    }
    
    var isAnyItemSelected: Bool {
        get {
            for (_, object) in self.dataSource.enumerated() {
                if object.isSelected ?? false {
                    return true
                }
            }
            return false
        }
    }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: rootDirectory.name )
        setBackButton()
        setRightBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllFiles()
        hideBottomView = true
        previewUrl = nil
    }
    
    //MARK:- Actions
    @IBAction func btnAddFolder(_ sender: Any) {
        showActionSheet()
    }
    
    @IBAction func btnMovetoFolder(_ sender: Any) {
        if isAnyItemSelected{
        showMoveToFolderViewController()
    }
    else{
    showAlerttoMoveOut()
    }
    }
    
    @IBAction func btnRename(_ sender: Any) {
        if isAnyItemSelected{
        showAddRenameAlert()
            
        }
        else{
            showAlerttoRename()
        }
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        if isAnyItemSelected{
            showDeleteAlert()
        }
        else{
            showAlerttoDelete()
        }
      
    }
    
    @objc func btnSelectAll() {
        
        switch isSelectedAll {
        case .edit:
            isSelectedAll = .cancel
            break
        case .cancel, .none:
            isSelectedAll = .edit
            break
        }
//        isSelectedAll = !isSelectedAll
    }
    
    @IBAction func didChangeTextField(textField: UITextField) {
        
        if textField == self.txtFolderName {
            guard let _alertAddFolder = self.alertAddFolder else { return }
            _alertAddFolder.actions[0].isEnabled = (textField.text ?? "").trim.count > 0
        }
        else if textField == self.txtRename{
            guard let _alertAddFolder = self.alertRename else { return }
            _alertAddFolder.actions[0].isEnabled = (textField.text ?? "").trim.count > 0
        }
        
    }
    
    //MARK:- Setter
    
    func setRightBarButton() {
        self.btnSelectedAll = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 28))
        btnSelectedAll?.setTitleColor(.white, for: .normal)
        btnSelectedAll?.addTarget(self, action: #selector(btnSelectAll), for: .touchUpInside)
        self.setRightBar(Button: btnSelectedAll!)
//        isSelectedAll = false
        isSelectedAll = .none
    }
    
    func setTextChangeEvent(at textField: UITextField) {
        textField.addTarget(self, action: #selector(didChangeTextField(textField:)), for: .editingChanged)
    }
    
    func setIsFolderSelected() {
        if isAnyFolderSelected {
            hideMoveToFolder = true
            hideRename = false
            
        } else {
            hideMoveToFolder = false
            hideRename = false
            
        }
    }
    
    func setIsMultipleFilesSelected() {
        var count = 0
        for (_, object) in self.dataSource.enumerated() {
            if object.isSelected ?? false && object.isDirectory ?? .folder == .file {
                count = count + 1
            }
        }
        
        if count == 0 {
            return
        }
//        if count >= 2 || isAnyFolderSelected
        if count >= 2 {
            hideRename = true
        } else {
            hideRename = false
        }
    }
    
    func setPreviewUrl(data: FileModel) {
        if let _documentDirectory = fileManager.documentDirectory {
            previewUrl = _documentDirectory.appendingPathComponent(rootDirectory.path).appendingPathComponent(data.fileId ?? "")
        }
    }
    
    
    //MARK:- Getter
    
    func getSelectedFile() -> [FileModel] {
        return dataSource.filter { (model) -> Bool in
            return model.isSelected ?? false
        }
    }
    
    func getAllFiles() {
        dataSource = []
        do {
            dataSource = try CoreDataManager.manager.getAllFilesBy(directory: rootDirectory.name)
        } catch {
        }
        btnSelectedAll?.isHidden = dataSource.count > 0 ? false : true
        foldersCollectionView.reloadData()
    }
   

    //MARK:- Helping Methods
    
    func createFolderBy(name: String, andId fileId: String) {
        fileManager.createDirectoryBy(name: fileId, inDirectory: rootDirectory.path) { [unowned self] (url, errorString) in
            guard let _ = url else {
                self.showAlertWithTitle(message: errorString ?? "\(name) directory not created") {
                }
                return
            }
            let folderExists = checkFolder(name: name)
            if folderExists{
                self.getAllFiles()
                return
            }
            else {
            self.saveFolderBy(name: name, andId: fileId, parentDirectory: rootDirectory)
            self.getAllFiles()
            }
        }
    }
    
    func checkFolder(name:String) -> Bool{
        
        do {
            dataSource = try CoreDataManager.manager.getAllFoldersBy(name: name)
            print(dataSource.count)
            if (dataSource.count > 0) {
                showAlertWithTitle(message: "Folder with same name exists already") {
                    self.dismiss()
                }
                return true
            }
           
        } catch {
            
        }
        return false
    }
    
    func saveFolderBy(name: String, andId fileId: String, parentDirectory: RootDirectory) {
        let fileModel = FileModel()
        fileModel.fileId = fileId
        fileModel.path = parentDirectory.path
        fileModel.name = name
        fileModel.parentDirectory = parentDirectory.name
        fileModel.isDirectory = .folder
        do {
            try CoreDataManager.manager.saveFile(file: fileModel)
        } catch {
        }
    }
    
    func saveFileBy(name: String, ext: String, andId fileId: String, parentDirectory: RootDirectory) {
        let fileModel = FileModel()
        fileModel.fileId = fileId
        fileModel.path = parentDirectory.path
        fileModel.name = name
        fileModel.parentDirectory = parentDirectory.name
        fileModel.isDirectory = .file
        do {
            try CoreDataManager.manager.saveFile(file: fileModel)
        } catch {
        }
    }
    
    func showAlerttoRename(){
        showAlertWithTitle(message: "Please select any File or Folder to rename") {
            self.dismiss()
        }
    }
    func showAlerttoDelete(){
        showAlertWithTitle(message: "Please select any File or Folder to delete") {
            self.dismiss()
        }
    }
    func showAlerttoMoveOut(){
        showAlertWithTitle(message: "Please select any File to moveout") {
            self.dismiss()
        }
    }
    
    func showDeleteAlert(){
        let alert = UIAlertController(title: "Are You Sure ?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        { [unowned self] (login) in
            deleteSelectedFiles(files: getSelectedFile())
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        okAction.isEnabled = true
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.alertDeleteFolder = alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAddFolderNameAlert() {
        let alert = UIAlertController(title: "Add New Folder", message: "Please enter name of folder", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Add", style: .default) { [unowned self] (login) in
            guard let _textField = self.txtFolderName, let _text = _textField.text else { return }
            self.createFolderBy(name: _text, andId: UUID().uuidString)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        alert.addTextField { [unowned self] (textField) in
            textField.placeholder = "Folder name"
            self.txtFolderName = textField
            self.setTextChangeEvent(at: self.txtFolderName ?? textField)
        }
        okAction.isEnabled = false
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.alertAddFolder = alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAddRenameAlert() {
        let alert = UIAlertController(title: "Rename", message: "Please enter name", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Rename", style: .default) { [unowned self] (login) in
            
            guard let _textField = self.txtRename, let _text = _textField.text else { return }
            self.renameFileBy(name: _text)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        alert.addTextField { [unowned self] (textField) in
            textField.placeholder = "Folder name"
            self.txtRename = textField
            self.setTextChangeEvent(at: self.txtRename ?? textField)
            
        }
        
        okAction.isEnabled = false
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.alertRename = alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDocumentPicker() {
//        var wordDoc = UTTypeCopyPreferredTagWithClass("com.microsoft.word.doc" as CFString, kUTTagClassFilenameExtension)
//        var openxmlformats: String =  "org.openxmlformats.wordprocessingml.document"
//
        if #available(iOS 14.0, *) {
            let documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: [.pdf, .fileURL,.text,.spreadsheet,.rtf,.appleArchive], asCopy: true)
            documentPicker.allowsMultipleSelection = true
            documentPicker.delegate = self
            self.present(documentPicker, animated: true)
        } else {
            let documentPicker = UIDocumentPickerViewController(documentTypes: ["application/pdf"], in: .import)
            documentPicker.allowsMultipleSelection = true
            documentPicker.delegate = self
            self.present(documentPicker, animated: true)
            // Fallback on earlier versions
        }
    //    let documentPicker = UIDocumentPickerViewController()
        

    }
    
    func showActionSheet() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Add New Folder", style: .default , handler:{ [unowned self] (UIAlertAction)in
            self.showAddFolderNameAlert()
        }))
        
        alert.addAction(UIAlertAction(title: "Import files from iCloud", style: .default , handler:{ [unowned self] (UIAlertAction)in
            self.showDocumentPicker()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive , handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func showRenameButton() -> Void {
        
        var count = 0
        for (_, object) in self.dataSource.enumerated() {
            if object.isSelected ?? false && object.isDirectory ?? .folder == .file {
                count = count + 1
            }
        }

//        if count >= 2 || isAnyFolderSelected
        if count >= 2 {
            btnRename.isHidden = true
        } else {
            btnRename.isHidden = false
        }
        
    }
    
    func showMoveToFolderButton() -> Void {
        
        if isAnyFolderSelected {
            btnMoveToFolder.isHidden = true
        } else {
            btnMoveToFolder.isHidden = false
        }
        
    }
    
    func copyImportFile(_url: [URL]) {
        for file in _url {
            let path = "\(UUID().uuidString).\(file.fileExtension)"
            guard let _fileDirectoryURL = fileManager.documentDirectory else {
                continue
            }
            let destinationPath = _fileDirectoryURL.appendingPathComponent(rootDirectory.path).appendingPathComponent(path)
            self.fileManager.copyFile(soucrePath: file, destinationPath: destinationPath)
//            self.fileManager.copyFile(file: file, to: .file, byName: path)
            saveFileBy(name: file.fileName, ext: file.fileExtension, andId: path, parentDirectory: rootDirectory)
        }
        getAllFiles()
    }
    
    
    func selectItemAtIndexPath(indexPath: IndexPath) {
        switch isSelectedAll {
        case .cancel, .none:
            let item = dataSource[indexPath.row]
            switch item.isDirectory ?? .folder {
            case .file:
                setPreviewUrl(data: dataSource[indexPath.row])
                let previewFileController = QLPreviewController()
                previewFileController.dataSource = self
                present(previewFileController , animated: true)
                break
            case .folder:
                showFoldersViewController(data: dataSource[indexPath.row])
                break
            }
            break
        default:
            dataSource[indexPath.row].isSelected = !(dataSource[indexPath.row].isSelected ?? false)
            isSelectedAll = .edit
            showRenameButton()
            showMoveToFolderButton()
            foldersCollectionView.reloadItems(at: [indexPath])
        }
    }
    
    func selectAllItems(isSelectAll: Bool) {
        
        let result = dataSource.compactMap { (model) -> FileModel? in
            model.isSelected = isSelectAll
            return model
        }
        self.dataSource = []
        self.dataSource = result
        foldersCollectionView.reloadData()
    }
    
    func deleteSelectedFiles(files: [FileModel] ) {
        
        let paths = files.compactMap { (model) -> String? in
            return model.path
        }
        fileManager.deleteDirectoriesBy(names: paths)
        CoreDataManager.manager.deleteFiles(files: files)
        hideBottomView = true
        getAllFiles()
    }
    
    func moveFiles(files: [FileModel], to directory: RootDirectory) {
        let paths = files.compactMap { (model) -> String? in
            return "\(model.path ?? "")/\(model.fileId ?? "")"
        }
        
        fileManager.moveDirectories(from: paths, to: directory.path)
        updateFilesPath(files: files, toDirectory: directory)
        getAllFiles()
    }
    
    func updateFilesPath(files: [FileModel], toDirectory directory: RootDirectory) {
        for file in files {
            do {
                try CoreDataManager.manager.updateFile(fileId: file.fileId ?? "", atDirectory: directory)
            } catch {
                print("Exception")
            }
        }
    }
    
    func renameFileBy(name: String) {
        guard let file = getSelectedFile().first, let fileId = file.fileId else {
            return
        }
        let folderExists = checkFolder(name: name)
        if folderExists{
            self.getAllFiles()
            return
        }
        else{
        do {
            try CoreDataManager.manager.updateFileName(fileId: fileId, name: name)
        } catch {
            self.showAlertWithTitle(message: "File name not updated") {
            }
        }
        getAllFiles()
        }
        
    }
    
    //MARK:- Router
    
    func showMoveToFolderViewController() {
        
        let controller = MoveToFolderViewController.initiateFrom(Storybaord: .browser)
        controller.selectedFolder = { [unowned self] (model: FileModel?) in
            
            guard let _model = model else {
                getAllFiles()
                return
            }
            
            var url: URL = URL(fileURLWithPath: "")
            var filePath: String = ""
            if let _documentDirectory = fileManager.documentDirectory {
                url = _documentDirectory.appendingPathComponent(_model.path ?? "").appendingPathComponent(_model.fileId ?? "")
                let rootPath = _documentDirectory.path
                filePath = url.path.replacingOccurrences(of: "\(rootPath)/", with: "")
            }
            self.moveFiles(files: self.getSelectedFile(), to: (name: _model.name ?? "", path: filePath))
//            self.moveFiles(files: self.getSelectedFile(), to: model.path ?? "")
        }
        controller.rootDirectory = rootDirectory
        let navController = UINavigationController(rootViewController: controller)
        self.present(navController, animated: true) {
        }
    }
    
    func showFoldersViewController(data: FileModel) {
        var url: URL = URL(fileURLWithPath: "")
        var finalPath = ""
        if let _documentDirectory = fileManager.documentDirectory {
            let rootPath = _documentDirectory.path
            url = _documentDirectory.appendingPathComponent(rootDirectory.path).appendingPathComponent(data.fileId ?? "")
            finalPath = url.path.replacingOccurrences(of: "\(rootPath)/", with: "")
        }

        let foldersViewController: FoldersViewController = FoldersViewController.initiateFrom(Storybaord: .browser)
        foldersViewController.rootDirectory = RootDirectory(name: data.name ?? "", path: finalPath)
        push(toController: foldersViewController, animated: true)
    }
    
    func showFileBrowserViewController(data: FileModel) {
        
        let fileBrowserViewController: FileBrowserViewController = FileBrowserViewController.initiateFrom(Storybaord: .browser)
        if let _documentDirectory = fileManager.documentDirectory {
            fileBrowserViewController.url = _documentDirectory.appendingPathComponent(rootDirectory.path).appendingPathComponent(data.path ?? "").absoluteString
        }
        push(toController: fileBrowserViewController, animated: true)
    }
}

//MARK:- UIDocumentPickerDelegate
extension FoldersViewController: UIDocumentPickerDelegate{
 
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        copyImportFile(_url: urls)
    }

}

//MARK:- QLPreviewControllerDataSource
extension FoldersViewController: QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        guard let _previewUrl = previewUrl else {
            showAlertWithTitle(message: "File preview not available") {
            }
            return URL(fileURLWithPath: "") as QLPreviewItem
        }
        return _previewUrl as QLPreviewItem
    }
}




//MARK:- UICollectionViewDelegate
extension FoldersViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectItemAtIndexPath(indexPath: indexPath)
    }
}

//MARK:- UICollectionViewDataSource
extension FoldersViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoldersCollectionViewCell", for: indexPath) as! FoldersCollectionViewCell
        let data = dataSource[indexPath.row]
        cell.showData(data: data)
        return cell
    }
    
}

//MARK:- UICollectionViewDelegateFlowLayout
extension FoldersViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.size.width - 42) / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //vertical padding
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        //horizontal padding
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 20, left: 10, bottom: 64, right: 10)
    }
}
