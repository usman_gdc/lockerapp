//
//  MoveToFolderViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 05/07/2021.
//

import UIKit

class MoveToFolderViewController: BaseViewController {
    //MARK:- IBOutlets
    
    @IBOutlet weak var filesCollectionView: UICollectionView!{
        didSet {
            filesCollectionView.dataSource = self
            filesCollectionView.delegate = self
            filesCollectionView.register(UINib(nibName: "FoldersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FoldersCollectionViewCell")
            
        }
    }
    
    //MARK:- Properties
    var alertAddFolder: UIAlertController?
    let fileManager: FileHadlingManager = FileHadlingManager()
    var dataSource: [FileModel] = []
    var btnFolder: UIButton?
    var txtFolderName: UITextField?
    var selectedFolder: ((_ model: FileModel?) -> Void) = { (_ model: FileModel?) in }
    var rootDirectory: RootDirectory = (name: "", path: "")
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationButton()
        setBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllFiles()
    }
    
    //MARK:- Actions
    @IBAction func btnCancel(sender: UIButton){
        dismissWithFolder(model: nil)
    }
    
    @IBAction func btnFolder(sender: UIButton){
        showAddFolderNameAlert()
    }
    
    @IBAction func didChangeTextField(textField: UITextField) {
        guard let _alertAddFolder = self.alertAddFolder else { return }
        _alertAddFolder.actions[0].isEnabled = (textField.text ?? "").trim.count > 0
    }

    //MARK:- Setter
    func setNavigationButton() {
        let btnCancel = UIButton(type: .custom)
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(.white, for: .normal)
        btnCancel.addTarget(self, action: #selector(btnCancel(sender:)), for: .touchUpInside)
        self.setLeftBar(Button: btnCancel)
        
        self.btnFolder = UIButton(type: .custom)
        btnFolder?.setImage(UIImage(named: "homeFileIcon"), for: .normal)
        btnFolder?.addTarget(self, action: #selector(btnFolder(sender:)), for: .touchUpInside)
        self.setRightBar(Button: btnFolder!)
        
    }
    
    func setTextChangeEvent(at textField: UITextField) {
        textField.addTarget(self, action: #selector(didChangeTextField(textField:)), for: .editingChanged)
    }
    
    func setSelectedFolder(model: FileModel?) {
        dismissWithFolder(model: model)
//        dismiss(animated: true) { [unowned self] in
//            self.selectedFolder(model)
//        }
    }
    
    //MARK:- Getter
    
    func getAllFiles() {
        dataSource = []
        do {
            dataSource = try CoreDataManager.manager.getFilesBy(category: .folder, fromDirectory: rootDirectory.name)
        } catch { }
        filesCollectionView.reloadData()
    }
    
    func showAddFolderNameAlert() {
        let alert = UIAlertController(title: "Add New Folder", message: "Please enter name of folder", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Add", style: .default) { [unowned self] (login) in
            
            guard let _textField = self.txtFolderName, let _text = _textField.text else { return }
            self.createFolderBy(name: _text, andId: UUID().uuidString)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (login) in
        }
        alert.addTextField { [unowned self] (textField) in
            textField.placeholder = "Folder name"
            self.txtFolderName = textField
            self.setTextChangeEvent(at: self.txtFolderName ?? textField)
            
        }
        
        okAction.isEnabled = false
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.alertAddFolder = alert
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Helping Methods
    
    func dismissWithFolder(model: FileModel?) {
        dismiss(animated: true) { [unowned self] in
            self.selectedFolder(model)
        }
    }

    
    func createFolderBy(name: String, andId fileId: String) {
        fileManager.createDirectoryBy(name: fileId, inDirectory: rootDirectory.path) { [unowned self] (url, errorString) in
            guard let _ = url else {
                self.showAlertWithTitle(message: errorString ?? "\(name) directory not created") {
                }
                return
            }
            let folderExists = checkFolder(name: name)
            if folderExists{
                self.getAllFiles()
                return
            }
            else {
            self.saveFolderBy(name: name, andId: fileId, parentDirectory: rootDirectory)
            self.getAllFiles()
            }
        }
    }
    
    func checkFolder(name:String) -> Bool{
        
        do {
            dataSource = try CoreDataManager.manager.getAllFoldersBy(name: name)
            print(dataSource.count)
            if (dataSource.count > 0) {
                showAlertWithTitle(message: "Folder with same name exists already") {
                    self.dismiss()
                }
                return true
            }
           
        } catch {
            
        }
        return false
    }
    

    func saveFolderBy(name: String, andId fileId: String, parentDirectory: RootDirectory) {
        let fileModel = FileModel()
        fileModel.fileId = fileId
        fileModel.path = parentDirectory.path
        fileModel.name = name
        fileModel.parentDirectory = parentDirectory.name
        fileModel.isDirectory = .folder
        do {
            try CoreDataManager.manager.saveFile(file: fileModel)
        } catch {
        }
    }


    
    //MARK:- Router
   
}


//MARK:- UICollectionViewDelegate
extension MoveToFolderViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        setSelectedFolder(model: dataSource[indexPath.row])
    }
}

//MARK:- UICollectionViewDataSource
extension MoveToFolderViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoldersCollectionViewCell", for: indexPath) as! FoldersCollectionViewCell
        let data = dataSource[indexPath.row]
        cell.showData(data: data)
        return cell
    }
    
}

//MARK:- UICollectionViewDelegateFlowLayout
extension MoveToFolderViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.size.width - 42) / 3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //vertical padding
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        //horizontal padding
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 20, left: 10, bottom: 0, right: 10)
    }
}
