//
//  AddAppsViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 22/06/2021.
//

import UIKit
import iTunesSearchAPI
import StoreKit
import CoreData
import SDWebImage



class AddAppsViewController: BaseViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var txtSearchApp: UITextField!
    @IBOutlet weak var collectionViewApps: UICollectionView! {
        didSet {
            collectionViewApps.dataSource = self
            collectionViewApps.delegate = self
            collectionViewApps.register(UINib(nibName: "ApplicationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ApplicationCollectionViewCell")
        }
    }
    
    
    
    //MARK:- Properties
    
    var appsArray = [AppsModel]()
    var searchedAppsArray = [AppsModel]()
    let itunesSearchManager = ITunesSearchManager()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchApp.delegate = self
        self.setBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- Actions
    
    @IBAction func btnSearch(_ sender: Any) {
        searchApp(searchText: txtSearchApp.text ?? "")
    }
    
    
    //MARK:- Setter
    
    func setAppsDataSource(data: [AppsModel]?) {
        guard let _data = data, _data.count > 0 else {
            self.appsArray = []
            collectionViewApps.reloadData()
            // Show no data found label
            return
        }
        self.appsArray = checkAppsFromDataBase(_data)
        collectionViewApps.reloadData()
    }
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func saveApp (application : AppsModel){
        
        do {
            try CoreDataManager.manager.saveApplication(app: application)
            print("Application Saved")
        } catch {

        }
    }
    
    func checkAppsFromDataBase(_ data: [AppsModel]) -> [AppsModel] {
        let result = data.compactMap { (appModel) -> AppsModel? in
            var model = appModel
            guard let _ = CoreDataManager.manager.getAppBybundleId(bundleId: appModel.bundleId ?? "")
            else {
                model.isExisted = false
                print("App already installed")
                return model
            }
            model.isExisted = true
            return model
            
            
        }
        return result
    }
    
    
    func searchApp(searchText: String) {
        
        itunesSearchManager.searchApp(appName: searchText) { [weak self] (result) in
            guard let `self` = self else { return }
            self.setAppsDataSource(data: result)
        }
    }
    
    func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        //        https://itunes.apple.com/in/app/<AppName>/id<AppID>?mt=8
        //        "https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
        // "https://apps.apple.com/us/app/facebook/id284882215?uo=4"
        
        if let url = URL(string: "https://apps.apple.com/us/app/facebook/id284882215?uo=4")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
        
    }
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Router
    
    func showAppViewController() {
        let vc: AppViewController = AppViewController.initiateFrom(Storybaord: .main)
        push(toController: vc, animated: true)
    }
    
}

//MARK:- Extensions

extension AddAppsViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Cell Clicked")
        
    }
}

extension AddAppsViewController: UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appsArray.count
    }
    
    @IBAction func btnAddApplication(_ sender: UIButton) {
        
        let appData = appsArray[sender.tag]
        if appData.isExisted ?? false {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ApplicationCollectionViewCell = collectionViewApps.dequeueReusableCell(withReuseIdentifier: "ApplicationCollectionViewCell", for: indexPath) as! ApplicationCollectionViewCell
        let appData = appsArray[indexPath.row]
        cell.setAppData(data: appData, at: indexPath , delegate: self)
        cell.delegate = self
//        cell.rowDidSelect = { (row) in
//        }
        
        return cell
    }
}

extension AddAppsViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.size.width - 30) / 3
        let height = width + width * 0.1
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //vertical padding
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        //horizontal padding
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 30, left: 15, bottom: 0, right: 15)
    }
}

extension AddAppsViewController: AddApplicationDelegate {
    
    func addApplication(At index: Int) {
        let appData = appsArray[index]
        print(appData)
        if appData.isExisted ?? false == false {
            appsArray[index].isExisted = true
            saveApp(application: appData)
            collectionViewApps.reloadData()
            self.pop()
           
        }
    }
}

extension AddAppsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        //textField code
        textField.resignFirstResponder()
//        searchText.resignFirstResponder()
        searchApp(searchText: txtSearchApp.text ?? "")
        return true
    }
    
}
