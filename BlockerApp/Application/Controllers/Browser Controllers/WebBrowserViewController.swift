//
//  WebBrowserViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 13/06/2021.
//

import UIKit

class WebBrowserViewController: BaseViewController  {
    
    //MARK:- IBOutlets
    @IBOutlet weak var browserTableView: UITableView! {
        didSet {
            browserTableView.dataSource = self
            browserTableView.delegate = self
        }
    }
    
    @IBOutlet weak var searchText: UITextField!
    
    
    //MARK:- Properties
    
    var browserArray = [BrowserModel]()
    
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchText.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setBackButton()
        getBrowserHistory()
        
    }
    
    
    //MARK:- Actions
    
    @IBAction func btnSearch(_ sender: Any) {
        saveBrowserHistory()
        if (searchText.text ?? "").count <= 0 {
            // Show alert
            return
        }
        displayWebpageViewControllerWith(seacrhText: searchText.text ?? "")
        
    }
    
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func saveBrowserHistory() {
        
        var model = BrowserModel()
        model.browserId = UUID().uuidString
        model.browserTitle = ""
        model.browserUrl = searchText.text
        do {
             try CoreDataManager.manager.saveBrowser(browser: model)
        } catch {
        }
        
    }
     
    func getBrowserHistory(){
        
        do {
            browserArray = try CoreDataManager.manager.getBrowserHistory()
            browserTableView.reloadData()
        } catch {
        }
    }
    func deleteBrowserHistory(browserId: String ) {
        
        do {
            try CoreDataManager.manager.deleteBrowserHistoryBy(browserId: browserId)
        } catch {
        }
        getBrowserHistory()
    }
    
    
    //MARK:- Router
    
    func displayWebpageViewControllerWith(seacrhText: String) {
        let vc: DisplayWebpageViewController = DisplayWebpageViewController.initiateFrom(Storybaord: .browser)
        vc.webUrl = seacrhText
        self.push(toController: vc)
    }

}

//MARK: Extensions

extension WebBrowserViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return browserArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cellId")
        
        cell.textLabel?.text = "Title"
        cell.detailTextLabel?.text = browserArray[indexPath.row].browserUrl
        cell.detailTextLabel?.textColor = UIColor.lightGray
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: nil) { [self] (_, _, completionHandler) in
            deleteBrowserHistory(browserId: browserArray[indexPath.row].browserId ?? "")
                completionHandler(true)
            }
        deleteAction.image = UIImage(named: "btnDeleteIcon")
        deleteAction.backgroundColor = UIColor.Color.lightpurpleappcolor
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            deleteBrowserHistory(browserId: browserArray[indexPath.row].browserId ?? "")
        }
    }
    
    
    
}

extension WebBrowserViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        displayWebpageViewControllerWith(seacrhText: browserArray[indexPath.row].browserUrl ?? "")
    }
    
}

extension WebBrowserViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        //textField code
        textField.resignFirstResponder()
//        searchText.resignFirstResponder()
        saveBrowserHistory()
        displayWebpageViewControllerWith(seacrhText: searchText.text ?? "")
        return true
    }
    
}

