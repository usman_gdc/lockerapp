//
//  DisplayWebpageViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 21/06/2021.
//

import UIKit
import WebKit

class DisplayWebpageViewController: BaseViewController, WKUIDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var lockerWebView: WKWebView! {
        didSet {
            lockerWebView.uiDelegate = self
        }
    }
    
    //MARK:- Properties
    
    var webUrl:String = ""
    
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        validateUrl()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
    }
    
    
    //MARK:- Actions
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func validateUrl() {
        if webUrl.isValidURL {
            loadWebpage()
        } else {
            startSearch()
        }
    }
    
    func loadWebpage(){
        var prefix = ""
        if !webUrl.hasPrefix("https://") || !webUrl.hasPrefix("http://") {
            prefix = "https://"
        }
        let myURL = URL(string: "\(prefix)\(webUrl)")
        let myRequest = URLRequest(url: myURL!)
        lockerWebView.load(myRequest)
        print("Working")
    }
    
    func loadHistory(){
        for page in lockerWebView.backForwardList.backList {
            print("User visited \(page.url.absoluteString)")
        }
    }
    
    func startSearch() {
        let allowedCharacters = NSCharacterSet.urlFragmentAllowed
        guard let  encodedSearchString  = webUrl.addingPercentEncoding(withAllowedCharacters: allowedCharacters)  else { return }

        let queryString = "https://www.google.de/search?q=\(encodedSearchString)"
        guard let queryURL = URL(string: queryString) else { return }

        let myRequest = URLRequest(url:queryURL)
            lockerWebView.load(myRequest)
    }
    
    
}




