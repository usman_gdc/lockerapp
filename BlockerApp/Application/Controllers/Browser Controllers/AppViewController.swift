//
//  AppViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 13/06/2021.
//

import UIKit
import SDWebImage
import CoreData


class AppViewController: BaseViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var searchBarApps: UISearchBar!

    @IBOutlet weak var tableViewApps: UITableView!
    
    //MARK:- Properties
    
    var appsArray = [AppsModel]()
    var searchedAppsArray = [AppsModel]()
    
    
    //MARK:- Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

     //   deleteAllApps()
        setBackButton()
        
        tableViewApps.dataSource = self
        tableViewApps.delegate = self
        searchBarApps.delegate = self
        
        tableViewApps.register(UINib(nibName: "ApplicationsTableViewCell", bundle: nil), forCellReuseIdentifier: "ApplicationsTableViewCell")
        
        self.tableViewApps.contentInset = UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApps()
    }
    
    //MARK:- Actions
    
    @IBAction func btnAddApp(_ sender: Any) {
        addAppsViewController()
        
    }
    
    //MARK:- Setter
    
    //MARK:- Getter
    
    //MARK:- Helping Methods

    func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        if let url = URL(string: identifier)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }

    }
    
    func fetchApps() {
        
        do {
            appsArray = try CoreDataManager.manager.fetchApps()
            
            searchedAppsArray = appsArray
        } catch {
        }
        tableViewApps.reloadData()
    }
    
    func deleteApps(bundleId: String ) {
        
        do {
            try CoreDataManager.manager.deleteAppsBy(bundleId: bundleId)
        } catch {
        }
        fetchApps()
    }
    
    func deleteAllApps() {
        
        do {
             try CoreDataManager.manager.deleteAllApps()
        } catch {
        }
        fetchApps()
    }

    //MARK:- Router
    
    func addAppsViewController() {
        let vc: AddAppsViewController = AddAppsViewController.initiateFrom(Storybaord: .browser)
            push(toController: vc, animated: true)
    }
    
    
}

//MARK:- Extensions

extension AppViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return searchedAppsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell: ApplicationsTableViewCell = self.tableViewApps.dequeueReusableCell(withIdentifier: "ApplicationsTableViewCell") as! ApplicationsTableViewCell
        
        cell.lblAppName.text = searchedAppsArray[indexPath.row].trackName
        cell.imageViewApp.sd_setImage(with: URL(string: searchedAppsArray[indexPath.row].artworkUrl60 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: nil) { [self] (_, _, completionHandler) in
            deleteApps(bundleId: appsArray[indexPath.row].bundleId ?? "")
                completionHandler(true)
            }
        deleteAction.image = UIImage(named: "btnDeleteIcon")
        deleteAction.backgroundColor = UIColor.Color.lightpurpleappcolor
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            deleteApps(bundleId: appsArray[indexPath.row].bundleId ?? "")
        }
    }

}

extension AppViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let appData = appsArray[indexPath.row]
        openStoreProductWithiTunesItemIdentifier(appData.trackViewUrl ?? "")
    }
}

extension AppViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchedAppsArray = appsArray
            
        }else {
            searchedAppsArray = appsArray.filter({($0.trackName)!.lowercased().contains(searchText.lowercased())})
        }
        self.tableViewApps.reloadData()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}



