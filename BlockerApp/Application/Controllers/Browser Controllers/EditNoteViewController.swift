//
//  EditNoteViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 09/07/2021.
//

import UIKit

class EditNoteViewController: BaseViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtNote: RSKPlaceholderTextView!
    
    
    
    //MARK:- Properties
    var noteDetail = NotesModel()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtTitle.text = noteDetail.title
        self.txtNote.text = noteDetail.note
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        txtNote.placeholder = "Note"
        txtNote.placeholderColor = UIColor.Color.placeHolderTextColor
        self.txtTitle.becomeFirstResponder()
        setBackButton()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(updateNote))
    }
    
    //MARK:- Actions
    
    
    //MARK:- Setter
    
    
    //MARK:- Getter
    
    //MARK:- Helping Methods
    
    func editNote(noteId: String , noteTitle: String, note: String){
        do {
            try CoreDataManager.manager.updateNotes(noteId: noteId, noteTitle: noteTitle, note: note)
        }catch{
            print("Error updating note")
        }
    }
    
    

    @objc func updateNote (){
        if txtTitle.text!.trim.count > 0{
            if txtNote.text!.trim.count > 0{
                editNote(noteId: noteDetail.noteId!, noteTitle: txtTitle.text!, note: txtNote.text)
                showAlertWithTitle(message: "Note is saved Successfully") {
                    self.goToNoteViewController()
                }
            }
            
            else
            {
                showAlertWithTitle(title: "Note's Description is Empty", message: "Please Enter Description", cancelTitle: "Cancel", otherTitle: "OK") {
                    self.txtNote.becomeFirstResponder()
                } otherButton: {
                    
                }
            }
        }
        
        else{
            
            showAlertWithTitle(title: "Note's Title is Empty", message: "Please Enter Title", cancelTitle: "Cancel", otherTitle: "OK") {
                self.txtTitle.becomeFirstResponder()
            } otherButton: {
                
            }

        }
        
    }
    
    
    
    
    //MARK:- Router
    
    func goToNoteViewController() {
       // self.navigationController?.popViewController(animated: true)
        self.pop()
    }

}
