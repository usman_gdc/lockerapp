//
//  ViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 03/06/2021.
//

import UIKit
import iTunesSearchAPI
import StoreKit

class ViewController: UIViewController, SKStoreProductViewControllerDelegate {
    

    let itunes: iTunes = iTunes(session: URLSession.shared, debug: true)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func printJSON(json: Any) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            
            print("\(String(data: jsonData, encoding: .utf8))")
            print()

        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func galleryBtn(_ sender: Any) {
        
        _ = itunes.search(for: "facebook", ofType: .software(.iPadSoftware), options: nil) { (result) in
            
//            guard let value = result.value as? String, let data = value.data(using: .utf8), let jsonArray = try? JSONSerialization.jsonObject(with: data, options : .allowFragments) else {
//                return
//            }
            guard let value = result.value else {
                return
            }
            self.printJSON(json: value)
//            print("\(jsonArray)")
//            print("result")
        }
        
        
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: FetchFromGalleryViewController = storyboard.instantiateViewController(identifier: "FetchFromGalleryViewController") as! FetchFromGalleryViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    @IBAction func nextBtn(_ sender: Any) {
        
        
        
   //     openStoreProductWithiTunesItemIdentifier("https://apps.apple.com/us/app/facebook/id284882215?uo=4")
        
//        openApp(appName: "facebook")
        
        
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: FacebookViewController = storyboard.instantiateViewController(identifier: "FacebookViewController") as! FacebookViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
//    func openApp(appName:String) {
//
//        let link = "https://apps.apple.com/us/app/facebook/id284882215?uo=4"
////        let appName = "instagram"
//  //      let appName = ""
//      //  let appScheme = "\(appName)://app"
//        let appUrl = URL(string: link)
//
//        if UIApplication.shared.canOpenURL(appUrl! as URL) {
//            UIApplication.shared.open(appUrl!)
//        } else {
//            print("App not installed")
//        }
//
//    }
    
    func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        
        if let url = URL(string: "https://itunes.apple.com/in/app/facebook/id284882215?mt=8")
        {
                   if #available(iOS 10.0, *) {
                      UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   }
                   else {
                         if UIApplication.shared.canOpenURL(url as URL) {
                            UIApplication.shared.openURL(url as URL)
                        }
                   }
        }
        
//        if let url = URL(string: "itms-apps://itunes.apple.com/app/d1546082813"),
//            UIApplication.shared.canOpenURL(url)
//        {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
        
//        if #available(iOS 10.0, *) {
//            let storeViewController = SKStoreProductViewController()
//            storeViewController.delegate = self
//
//            let parameters = [ SKStoreProductParameterITunesItemIdentifier : "284882215"]
//            storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
//                if loaded {
//                    // Parent class of self is UIViewContorller
//                    self?.present(storeViewController, animated: true, completion: nil)
//                }
//            }
//
//        } else {
//            UIApplication.shared.openURL(URL(string: "itms-apps://apple.com/app/id284882215")!)
//        }


        
        
//        if let url = URL(string: "itms-apps://apple.com/app/id284882215") {
//            UIApplication.shared.open(url)
//        }
//        let storeViewController = SKStoreProductViewController()
//        storeViewController.delegate = self
//
//        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
//        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
//            if loaded {
//                // Parent class of self is UIViewContorller
//                self?.present(storeViewController, animated: true, completion: nil)
//            }
//        }
    }
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

    
    
    
}
    
// Codable



