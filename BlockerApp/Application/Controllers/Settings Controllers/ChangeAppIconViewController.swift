//
//  ChangeAppIconViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 16/06/2021.
//

import UIKit

class ChangeAppIconViewController: BaseViewController {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var appIconCollectionView: UICollectionView! {
        didSet {
            appIconCollectionView.dataSource = self
            appIconCollectionView.delegate = self
            appIconCollectionView.register(UINib(nibName: "ChangeAppIconCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChangeAppIconCollectionViewCell")
        }
    }
    
    
    //MARK:- Properties
    var userRatedAppIconCheck:Bool = false
    var dataSource = [String]()
    var appIconIndex: Int?
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        setDataSource()
    }
    
    
    
    //MARK:- Actions
    
    //MARK:- Setter
    
    func setDataSource() {
        dataSource = ["changeiconapp1", "changeiconapp2", "changeiconapp3", "changeiconapp4", "changeiconapp5", "changeiconapp6", "changeiconapp7", "changeiconapp8", "changeiconapp9", "changeiconapp10", "changeiconapp11", "changeiconapp12", "changeiconapp13"]
        getAppIconIndex()
        appIconCollectionView.reloadData()
    }
    
    //MARK:- Getter
    
    func getAppIconIndex() {
        appIconIndex = nil
        guard let _iconName = UserDefaults.standard.appIconName else {
            return
        }
        for (index, name) in dataSource.enumerated() {
            if _iconName == name {
                appIconIndex = index
            }
        }
    }
    
    func setAppIconIndex(index: Int) {
        
        guard let _appIconIndex = appIconIndex else {
            changeIcon(to: dataSource[index])
            return
        }
        guard index < dataSource.count, _appIconIndex != index else {
            changeIcon(to: nil)
            return
        }
        changeIcon(to: dataSource[index])
    }
    
    
    //MARK:- Helping Methods
    
    func changeIcon(to name: String?) {
        UIApplication.shared.changeAppIcon(to: name) { [unowned self] (errorString) in
            guard let _msg = errorString else {
                UserDefaults.standard.appIconName = name
                userRatedAppIconCheck = UserDefaults.standard.userRatedAppIcon
                if userRatedAppIconCheck {
                    UserDefaults.standard.userRatedAppIcon = false
                    self.getAppIconIndex()
                    appIconCollectionView.reloadData()
                    return
                }
                else{
                    RateAndReviewApp.rateApp()
                    UserDefaults.standard.userRatedAppIcon = true
                    self.getAppIconIndex()
                }
                self.appIconCollectionView.reloadData()
                return
            }
            self.showAlertWithTitle(message: _msg) { }
            
        }
    }
    
    //MARK:- Router
    
}

//MARK:- Extensions

extension ChangeAppIconViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        setAppIconIndex(index: indexPath.row)
    }
}

extension ChangeAppIconViewController: UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = appIconCollectionView.dequeueReusableCell(withReuseIdentifier: "ChangeAppIconCollectionViewCell", for: indexPath) as! ChangeAppIconCollectionViewCell
        cell.imageView.image = UIImage(named: dataSource[indexPath.row] )
        cell.viewTick.isHidden = !(indexPath.row == appIconIndex ?? -1)
        return cell
    }
}

extension ChangeAppIconViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.size.width - 90
        return CGSize(width: width/4, height: width/4)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //vertical padding
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        //horizontal padding
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 20, left: 25, bottom: 0, right: 25)
    }
}
