//
//  SettingsViewController.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 15/06/2021.
//

import UIKit

class SettingsViewController: BaseViewController {
    
    //MARK:- typealias
    typealias PassCodeType = (passCode: Bool, pattern: Bool, faceId: Bool)
    //MARK:- IBOutlets
    @IBOutlet weak var swUnlockPassword: UISwitch!
    @IBOutlet weak var swfaceID: UISwitch!
    @IBOutlet weak var swPattern: UISwitch!
    @IBOutlet weak var swautoLockSecureFolder: UISwitch!
    
    //MARK:- Properties
    var passCode: PassCodeType = (passCode: false, pattern: false, faceId: false)
    var passCodeValue: Bool = false {
        didSet {
            swUnlockPassword.setOn(passCodeValue, animated: true)
            swUnlockPassword.isUserInteractionEnabled = !passCodeValue
        }
    }
    
    var patternValue: Bool = false {
        didSet {
            swPattern.setOn(patternValue, animated: true)
            swPattern.isUserInteractionEnabled = !patternValue
        }
    }
    
    var faceIdValue: Bool = false {
        didSet {
            swfaceID.setOn(faceIdValue, animated: true)
            swfaceID.isUserInteractionEnabled = !faceIdValue
        }
    }
    
    //MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCurrentPassCode()
        setCurrentPassCode()
        
    }
    
    //MARK:- Actions
    
    @IBAction func lockTypeValueChanged(_ sender: UISwitch) {
        
        switch sender {
        
        case swPattern:
            presentPassCodeViewControllerSettings { [unowned self] (isSuccess) in
                self.showOldPatternViewController()
                self.getCurrentPassCode()
                self.setCurrentPassCode()
            }
        case swfaceID:
            presentPassCodeViewControllerSettings { [unowned self] (isSuccess) in
                self.showFaceIDViewController()
                self.getCurrentPassCode()
                self.setCurrentPassCode()
            }

        default:
            presentPassCodeViewControllerSettings { [unowned self] (isSuccess) in
                self.showOldPasswordViewController()
                self.getCurrentPassCode()
                self.setCurrentPassCode()
            }
        }
    }
    
    @IBAction func advanceValueChanged(_ sender: UISwitch) {
    }
    
    @IBAction func btnChangeAppIcon(_ sender: Any) {
        changeAppIconViewController()
    }
    
    
    @IBAction func btnRateUs(_ sender: Any) {
        RateAndReviewApp.rateApp()
    }
    
    
    //MARK:- Setter
    
    func setCurrentPassCode() {
        if passCode.faceId {
            passCodeValue = false
            patternValue = false
            faceIdValue = true
        }
        else if passCode.pattern {
            passCodeValue = false
            patternValue = true
            faceIdValue = false
        }
        else  {
            passCodeValue = true
            patternValue = false
            faceIdValue = false
        }
    }
    
    //MARK:- Getter
    
    func getCurrentPassCode() {
        
        guard let _passCode = UserDefaults.standard.appPassCode, let _passCodeType = _passCode.passCodeType else {
            self.passCode = (passCode: true, pattern: false, faceId: false)
            return
        }
        switch _passCodeType {
        case .faceId:
            self.passCode = (passCode: false, pattern: false, faceId: true)
        case .pattern:
            self.passCode = (passCode: false, pattern: true, faceId: false)
        default:
            self.passCode = (passCode: true, pattern: false, faceId: false)
        }
    }
    
    //MARK:- Helping Methods
    
    //MARK:- Router
    
    func changeAppIconViewController() {
        let vc: ChangeAppIconViewController = ChangeAppIconViewController.initiateFrom(Storybaord: .settings)
           self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showOldPasswordViewController() {
        let controller: EnterPasswordViewController = EnterPasswordViewController.initiateFrom(Storybaord: .main)
        controller.showCancelBtn = true
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
    }
    
    func showOldPatternViewController() {
        let controller: NewPatternViewController = NewPatternViewController.initiateFrom(Storybaord: .main)
        controller.showCancelBtn = true
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
    }
    func showFaceIDViewController() {
        let controller: FaceIDViewController = FaceIDViewController.initiateFrom(Storybaord: .main)
        controller.showCancelBtn = true
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            
        }
    }
    
     func presentPassCodeViewControllerSettings(isLockSuccessful: ((_ isSuccess: Bool) -> Void)? = nil) {
        
        guard let _passCode = UserDefaults.standard.appPassCode, let _passCodeType = _passCode.passCodeType else {
            self.passCode = (passCode: true, pattern: false, faceId: false)
            return
        }
        switch _passCodeType {
        case .faceId:
            let controller = FaceIDViewController.initiateFrom(Storybaord: .main)
            controller.showCancelBtn = true
            controller.isLockSuccessful = { (isSuccess) in
                guard let _isLockSuccessful = isLockSuccessful else { return }
                _isLockSuccessful(isSuccess)
            }
            let navController = UINavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true) {
                
            }

        case .pattern:
            let controller = PatternViewController.initiateFrom(Storybaord: .main)
            controller.showCancelBtn = true
            controller.isLockSuccessful = { (isSuccess) in
                guard let _isLockSuccessful = isLockSuccessful else { return }
                _isLockSuccessful(isSuccess)
            }
            let navController = UINavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true) {
                
            }

        default:
            let controller = PasscodeViewController.initiateFrom(Storybaord: .main)
            controller.showBackBtnPasscode = true
            controller.isLockSuccessful = { (isSuccess) in
                guard let _isLockSuccessful = isLockSuccessful else { return }
                _isLockSuccessful(isSuccess)
            }
            let navController = UINavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true) {
                
            }

        }
        
    }
    
}

//MARK:- BaseViewController
extension SettingsViewController {
    
    
    
}

