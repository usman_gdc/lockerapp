//
//  ApplicationCollectionViewCell.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 22/06/2021.
//

import UIKit

protocol AddApplicationDelegate: AnyObject {
    func addApplication(At index: Int)
}

class ApplicationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewAppImage: UIImageView!
    @IBOutlet weak var lblAppname: UILabel!
    @IBOutlet weak var btnAppOutlet: UIButton!
    
    weak var delegate: AddApplicationDelegate?
    
    // closure
//    var rowDidSelect: (_ index: Int) -> Void = { (index: Int) in
//
//    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction private func btnAddApp(_ sender: UIButton) {
        
  //      rowDidSelect(sender.tag)
        
        guard let _delegate = delegate else {
            return
        }
        _delegate.addApplication(At: sender.tag)
        
    }
    
    func setAppData(data: AppsModel, at indexPath: IndexPath, delegate: UIViewController) {
        
        self.btnAppOutlet.tag = indexPath.row
        self.lblAppname.text = data.trackName
        self.imageViewAppImage.sd_setImage(with: URL(string: data.artworkUrl60 ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        if data.isExisted ?? false == true {
          //  self.btnAppOutlet.setBackgroundImage(UIImage(named: "welcome") ,for : UIControl.State.normal)
            self.btnAppOutlet.isHidden = true
            
        }
        
    }


}
