//
//  CollectionViewCellAddVideos.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 29/06/2021.
//

import UIKit
import SDWebImage
import SDWebImagePhotosPlugin

class CollectionViewCellAddVideos: UICollectionViewCell {

    @IBOutlet weak var imageViewVideo: UIImageView!
    @IBOutlet weak var viewTick: UIView!
    @IBOutlet weak var imgTick: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, data: PhotoAssetModel, photoManager: PhotoManager) -> CollectionViewCellAddVideos {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellAddVideos", for: indexPath) as! CollectionViewCellAddVideos
        let _isSelected = data.isSelected ?? false
        cell.viewTick.isHidden = !_isSelected
        SDImageLoadersManager.shared.loaders = [SDWebImageDownloader.shared, SDImagePhotosLoader.shared]
        SDImagePhotosLoader.shared.requestImageAssetOnly = false
        SDWebImageManager.defaultImageLoader = SDImageLoadersManager.shared
       
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        SDImagePhotosLoader.shared.fetchOptions = fetchOptions
        if let _img = data.image {
            cell.imageViewVideo.image = _img
        } else {
            if let asset = data.asset {
                let photosURL = NSURL.sd_URL(with: asset) as URL
                let requestOptions = PHImageRequestOptions()
                requestOptions.isNetworkAccessAllowed = true
                let manager = SDWebImageManager(cache: SDImageCache.shared, loader: SDImagePhotosLoader.shared)
                cell.imageViewVideo.sd_setImage(with: photosURL, placeholderImage: nil, options: .continueInBackground, context: [.photosImageRequestOptions: requestOptions, .customManager: manager])
            }
        }
        return cell
    }
    
    class func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, data: VideoModel, fileManager: FileHadlingManager) -> CollectionViewCellAddVideos {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellAddVideos", for: indexPath) as! CollectionViewCellAddVideos
        let _isSelected = data.isSelected ?? false
        cell.viewTick.isHidden = !_isSelected
        
        if let _photosUrl = fileManager.thumbnailDirectoryURL {
            cell.imageViewVideo.sd_setImage(with: _photosUrl.appendingPathComponent(data.thumbnail ?? "")) { (image, error, cache, url) in
            }
        }
        

        return cell
    }

}
