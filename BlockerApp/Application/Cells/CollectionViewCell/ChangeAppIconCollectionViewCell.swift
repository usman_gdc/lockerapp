//
//  ChangeAppIconCollectionViewCell.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 16/06/2021.
//

import UIKit

class ChangeAppIconCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var viewTick: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
