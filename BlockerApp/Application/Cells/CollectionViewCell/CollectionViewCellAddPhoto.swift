//
//  CollectionViewCellAddPhoto.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 28/06/2021.
//

import UIKit
import SDWebImage
import SDWebImagePhotosPlugin

class CollectionViewCellAddPhoto: UICollectionViewCell {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewTick: UIView!
    @IBOutlet weak var imgTick: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
        class func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, data: PhotoAssetModel, photoManager: PhotoManager) -> CollectionViewCellAddPhoto {
    
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellAddPhoto", for: indexPath) as! CollectionViewCellAddPhoto
            let _isSelected = data.isSelected ?? false
            cell.viewTick.isHidden = !_isSelected
            
            // Supports HTTP URL as well as Photos URL globally
            SDImageLoadersManager.shared.loaders = [SDWebImageDownloader.shared, SDImagePhotosLoader.shared]
            // Replace default manager's loader implementation
            SDWebImageManager.defaultImageLoader = SDImageLoadersManager.shared
            
            if let _img = data.image {
                cell.imageView.image = _img
            } else {
                if let asset = data.asset {
                    
                    let photosURL = NSURL.sd_URL(with: asset) as URL
                    cell.imageView.sd_setImage(with: photosURL, placeholderImage: nil) { (image, error, cache, url) in
                        data.image = image
                    }
                }
            }
            
            return cell
        }
    
    class func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, data: PhotoModel, fileManager: FileHadlingManager) -> CollectionViewCellAddPhoto {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellAddPhoto", for: indexPath) as! CollectionViewCellAddPhoto
        let _isSelected = data.isSelected ?? false
        cell.viewTick.isHidden = !_isSelected
        
        if let _photosUrl = fileManager.photoDirectoryURL {
            cell.imageView.sd_setImage(with: _photosUrl.appendingPathComponent(data.path ?? "")) { (image, error, cache, url) in
            }
        }
        return cell
    }

}
