//
//  FoldersCollectionViewCell.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 01/07/2021.
//

import UIKit



class FoldersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblFolderName: UILabel!
    @IBOutlet weak var imageViewFolder: UIImageView!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var viewBackgroundFolder: UIView!
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    func showData(data: FileModel) {
        self.lblFolderName.text = data.name
        self.imgTick.isHidden = !(data.isSelected ?? false)
        let isDirectory = data.isDirectory ?? .folder
        
        switch isDirectory {
        case .file:
            self.viewBackgroundFolder.backgroundColor = UIColor.Color.fileBackgroundColor
//            lblFolderName.textColor = .blue
            self.imageViewFolder.image = UIImage(named: "FileIcon")
            break
        case .folder:
            self.viewBackgroundFolder.backgroundColor = UIColor.Color.folderBackgroundColor
//            lblFolderName.textColor = .blue
            self.imageViewFolder.image = UIImage(named: "FolderIcon")
            break
        }
    }
    
   
}
