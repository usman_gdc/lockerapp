////
////  ServiceManager.swift
////  iOSApplication
////
////  Created by Usman Javaid on 06/01/2020.
////  Copyright © 2020 Incubators. All rights reserved.
////
//
//import Foundation
////import Alamofire
//
//class ServiceManager {
//    
//    @discardableResult
//    private func requestWith(url: String = AppConfig.baseUrl, method: HTTPMethod, headers: HTTPHeaders, parameters: Parameters?, encoding: ParameterEncoding = URLEncoding.default , completion: @escaping (_ response: ResponseResult) -> Void) -> DataRequest {
//        let request: DataRequest = AF.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers, interceptor: nil).validate(statusCode: [200, 400,500, 404]).responseJSON { response in
//            
//            debugPrint("URL: \(url)")
//            debugPrint("Headers: \(headers)")
//            debugPrint("Parameters: \(parameters ?? [:])")
//            debugPrint("Response: \(response.value ?? "Nil")")
//            guard let _data = response.data else {
//                completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.invalidURL])))
//                return
//            }
//            switch response.result {
//            case .success(_):
//                completion(.success(_data))
//                break
//            case .failure(_):
//                completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.invalidURL])))
//                break
//            }
//        }
//        return request
//    }
//    
//    @discardableResult
//    private func requestWith(urlRequest _request: URLRequest, completion: @escaping (_ response: ResponseResult) -> Void) -> DataRequest {
//        
//        let request: DataRequest = AF.request(_request).validate(statusCode: [200, 400,500, 404]).responseJSON { (response) in
//            debugPrint("URLRequest: \(_request)")
//            if let _body = _request.httpBody {
//                debugPrint("Parameters: \(String(bytes: _body, encoding: .utf8) ?? "NIL")")
//            }
//            debugPrint("Response: \(response.value ?? "Nil")")
//            guard let _data = response.data else {
//                completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.invalidURL])))
//                return
//            }
//            switch response.result {
//            case .success(_):
//                completion(.success(_data))
//                break
//            case .failure(_):
//                completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.invalidURL])))
//                break
//            }
//        }
//        return request
//    }
//    
//    @discardableResult
//    private func uploadFileWith(Data _data:Data, url: String, parameters: [String: String], headers:[String: String], completion: @escaping (_ response: ResponseResult) -> Void) -> DataRequest {
//        
//        let url = URL(string: url)!
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        for (key, value) in headers {
//            request.setValue(value, forHTTPHeaderField: key)
//        }
//
//        return AF.upload(multipartFormData: { (multipartFormData) in
//            for (key, value) in parameters {
//                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//            }
//            multipartFormData.append(_data, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
//        }, with: request).responseJSON(completionHandler: { (response) in
//            debugPrint("URLRequest: \(request)")
//            debugPrint("Response: \(response.value ?? "Nil")")
//            guard let _data = response.data else {
//                completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.invalidURL])))
//                return
//            }
//        
//            switch response.result {
//            case .success(_):
//                completion(.success(_data))
//                break
//            case .failure(_):
//                completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.invalidURL])))
//                break
//            }
//        })
//    }
//    
//    private func getHeaders(APIName apiName: String, contentType: String = "application/json") -> HTTPHeaders {
//        return ["ak": AppConfig.apiKey, "an": apiName, "av": AppConfig.apiVersion ,"Content-Type": contentType]
//    }
//    
//    private func convertToJSONString<T: Codable>(_ model: T) -> String {
//        return model.toJSONString
//    }
//}
//
//extension ServiceManager {
//    
//    func signin(User user: SigninModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(user)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.signIn)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(SigninResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//    
//    func signUp(WithUser user: SignupModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(user)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.signUp)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(SigninResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//    
//    func threadList(page: ThreadsPageModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(page)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.threadList)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(ThreadListResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//
//    func threaddetail(threadIdModel: ThreadlIdModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(threadIdModel)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.threadDetail)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(ThreadDetailResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//
//    func addComment(addCommentModel: AddCommentModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(addCommentModel)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.addComment)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(AddCommentResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//    
//    func addCommentReply(addReplyModel: AddReplyModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(addReplyModel)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.addReplyAgainstComment)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(AddCommentResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//
//    func creatNewThreadBy(createThreadModel: CreateThreadModel, completion: @escaping(ResponseResult) -> Void ) -> Void {
//
//        guard Connectivity.isConnected == true else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.noInternet])))
//            return
//        }
//
//        let parameters: Parameters = ["EValues": self.convertToJSONString(createThreadModel)]
//        guard let url = URL(string: AppConfig.baseUrl) else {
//            completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//            return
//        }
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addHeaders(WithAPIName: AppConfig.APIName.creatThread)
//        request.httpBody = parameters.toData
//
//        self.requestWith(urlRequest: request) { (resut) in
//            switch (resut) {
//                case .success(let responseData):
//                    guard let _responseData: Data = responseData as? Data, let _object: [String: Any] = _responseData.toJSONObject as? [String: Any], let _value = _object["eValues"] as? String ,let _data = _value.toData else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    guard let model = try? JSONDecoder().decode(CreatThreadResponseModel.self, from: _data) else {
//                        completion(.error(NSError(domain: "", code: 0, userInfo: ["message": ErrorMessages.defaultError])))
//                        return
//                    }
//                    completion(.success(model))
//                    break
//                case .error(let error):
//                    completion(.error(error))
//                    break
//            }
//        }
//    }
//
//}
