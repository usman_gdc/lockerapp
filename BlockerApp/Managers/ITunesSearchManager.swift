//
//  iTunesSearchApi.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 23/06/2021.
//

import Foundation
import iTunesSearchAPI
import StoreKit
//MARK:- Properties



class ITunesSearchManager {
    
    let itunes: iTunes = iTunes(session: URLSession.shared, debug: true)
    
    func searchApp(appName: String, completion: @escaping( [AppsModel]?) -> Void){
        _ = itunes.search(for: appName, ofType: .software(.iPadSoftware), options: nil) { (result) in
            guard let value = result.value, let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted), let response = try? JSONDecoder().decode(ITuneAppsResponeModel.self, from: jsonData) else {
                completion(nil)
                return
            }
            completion(response.results)
        }
    }
    
    func printJSON(json: Any) {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let appDetails = try! JSONDecoder().decode(ITuneAppsResponeModel.self, from: jsonData)
            print(appDetails)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        //        https://itunes.apple.com/in/app/<AppName>/id<AppID>?mt=8
//        "https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
        
        if let url = URL(string: "https://itunes.apple.com/in/app/Messenger/id454638411?mt=8")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }

    }
    
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
