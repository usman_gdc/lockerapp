//
//  PhotoManager.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 28/06/2021.
//

import Foundation
import Photos
import PhotosUI


class PhotoManager {
    
    enum AssetType {
        case image
        case video
    }
    
    let imageManager = PHImageManager.default()
    
    func photoAuthorization(completion: @escaping (_ status: PHAuthorizationStatus) -> Void) {
        // 1
        let status = PHPhotoLibrary.authorizationStatus()

        switch status {
        case .authorized, .limited:
            completion(status)
        case .restricted, .denied:
            completion(status)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized, .limited:
                    completion(status)
                case .restricted, .denied:
                    completion(status)
                case .notDetermined:
                    completion(status)
                default:
                    completion(status)
                }
            }
        default:
            completion(status)
        }
    }
    
    
    
    func loadPhotos(completion: @escaping (_ photos: [PhotoAssetModel]) -> Void) {
        var photos: [PhotoAssetModel] = []
//        let requestOptions = PHImageRequestOptions()
//         requestOptions.isSynchronous = true
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        fetchResult.enumerateObjects { (asset, index, mutablePointer) in
            let model = PhotoAssetModel()
            model.isSelected = false
            model.asset = asset
            photos.append(model)
        }
        completion(photos)
    }
    
    
    func loadVideos(completion: @escaping (_ photos: [PhotoAssetModel]) -> Void) {
        var videos: [PhotoAssetModel] = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        fetchResult.enumerateObjects { (asset, index, mutablePointer) in
            let model = PhotoAssetModel()
            model.isSelected = false
            model.asset = asset
            videos.append(model)
        }
        completion(videos)
    }
    
    func loadPhotosBy(identifiers:[String], completion: @escaping (_ photos: [PhotoAssetModel]) -> Void) {
        var photos: [PhotoAssetModel] = []
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(withLocalIdentifiers: identifiers, options: fetchOptions)
        fetchResult.enumerateObjects { (asset, index, mutablePointer) in
            let model = PhotoAssetModel()
            model.isSelected = false
            model.asset = asset
            photos.append(model)
        }
        completion(photos)
    }
    
    func loadMediaFrom(asset: PhotoAssetModel, preferredSize: CGSize, completion: @escaping (_ image: UIImage?) -> Void) {
        
        guard let _asset = asset.asset else {
            completion(nil)
            return
        }
        var imageRequestOptions: PHImageRequestOptions {
            let options = PHImageRequestOptions()
            options.version = .current
            options.resizeMode = .exact
            options.deliveryMode = .highQualityFormat
            options.isNetworkAccessAllowed = true
            options.isSynchronous = true
            return options
        }
        imageManager.requestImage(for: _asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: imageRequestOptions) { (image, hastable) in
            completion(image)
        }
    }
    
    func loadMediaFrom(asset: PhotoAssetModel, completion: @escaping (_ image: UIImage?) -> Void) {
        
        guard let _asset = asset.asset else {
            completion(nil)
            return
        }
        
        imageManager.requestImageDataAndOrientation(for: _asset, options: .none) { (data, string, orientation, hashtable) in
            guard let _data = data, let image = UIImage(data: _data) else {
                completion(nil)
                return
            }
            completion(image)
        }
    }
    
    func deleteAsset(asset: PhotoAssetModel, completion: @escaping(_ isSuccess: Bool) -> Void) {
        guard let _asset = asset.asset else { return }
        PHPhotoLibrary.shared().performChanges {
            PHAssetChangeRequest.deleteAssets(NSArray(object: _asset))
        } completionHandler: { (isSuccess, error) in
            completion(isSuccess)
        }
    }

    func deleteAssets(assets: [PhotoAssetModel], completion: @escaping(_ isSuccess: Bool) -> Void) {
        
        let phassets = assets.compactMap { (model) -> PHAsset? in
            guard let _asset = model.asset else { return nil }
            return _asset
        }
        
        PHPhotoLibrary.shared().performChanges {
            PHAssetChangeRequest.deleteAssets(NSArray(array: phassets))
        } completionHandler: { (isSuccess, error) in
            completion(isSuccess)
        }
    }
    
    func getUrlOf(asset: PHAsset, foType mediaType: PhotoManager.AssetType = .image  ,completion: @escaping(_ url: URL?) -> Void) {
        
        switch mediaType {
        case .image:
            asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (eidtingInput, info) in
                guard let input = eidtingInput, let photoUrl = input.fullSizeImageURL else {
                    completion(nil)
                    return
                }
                completion(photoUrl)
            }
        case .video:
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
                    options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: asset, options: options) { (videoAsset, audioMix, info) in
                guard let _videoAsset = videoAsset as? AVURLAsset else {
                    completion(nil)
                    return
                }
                completion(_videoAsset.url)
            }
        }
    }

}
