//
//  AdMobManager.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 30/08/2021.
//

import Foundation
import GoogleMobileAds

class AdMobManager:NSObject {
    
    enum AdType {
        case interstitial
        case banner
        case reward
    }
    
    typealias AdMobCompletionHandler = (Bool, Int?) -> Void
    
    fileprivate let interstitialAdId: String = "ca-app-pub-3940256099942544/4411468910"
    fileprivate let bannerAdId: String = "ca-app-pub-3940256099942544/2934735716"
    fileprivate let rewardAdId: String = "ca-app-pub-3940256099942544/1712485313"
    fileprivate var interstitialAd: GADInterstitialAd?
    fileprivate var rewardedAd: GADRewardedAd?
    fileprivate var adMobCompletionHandler: AdMobCompletionHandler?
    fileprivate var adType: AdType?
    fileprivate var rewardedPoints: Int?
    
    
    func loadInterstitialAd(from controller: UIViewController, completion: @escaping AdMobCompletionHandler)  {
        
        controller.showHud("Loading...")
        GADInterstitialAd.load(withAdUnitID: interstitialAdId, request: GADRequest()) { (ad, error) in
            
            controller.hideHUD()
            guard let _ = error else {
                self.adType = .interstitial
                self.interstitialAd = ad
                self.interstitialAd?.fullScreenContentDelegate = self
                self.interstitialAd?.present(fromRootViewController: controller)
                self.adMobCompletionHandler = completion
                return
            }
        }
    }
    
    func loadBannerAd(viewBannerAd : GADBannerView, in controller: UIViewController) {
        viewBannerAd.adUnitID = bannerAdId
        viewBannerAd.rootViewController = controller
        viewBannerAd.load(GADRequest())
        adType = .banner
    }
    
    func loadRewardedAd(in controller: UIViewController, completion: @escaping AdMobCompletionHandler) {
        
        controller.showHud("Loading...")
        GADRewardedAd.load(
            withAdUnitID: rewardAdId, request: GADRequest()
        ) { [weak self] (ad, error) in
            
            controller.hideHUD()
            guard let `self` = self else { return }
            self.adMobCompletionHandler = completion
            if let _ = error {
                self.callAdMobCompletionHandler(isComplete: false, value: nil)
                return
            }
            self.adType = .reward
            self.rewardedAd = ad
            self.rewardedAd?.fullScreenContentDelegate = self
            
            self.rewardedAd?.present(fromRootViewController: controller, userDidEarnRewardHandler: {
                let reward =  self.rewardedAd?.adReward
                self.rewardedPoints = NSInteger(truncating: reward?.amount ?? 0)
            })
        }
    }
    
    fileprivate func callAdMobCompletionHandler(isComplete: Bool, value: Int?) {
        guard let _adMobCompletionHandler = adMobCompletionHandler else {
            return
        }
        _adMobCompletionHandler(isComplete, value)
    }
    
}

extension AdMobManager : GADFullScreenContentDelegate {
    
    func adDidRecordClick(_ ad: GADFullScreenPresentingAd) {
        
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        
    }
    
    func adDidRecordImpression(_ ad: GADFullScreenPresentingAd) {
        
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        callAdMobCompletionHandler(isComplete: false, value: nil)
    }
    
    func adWillDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        if self.adType ?? AdType.interstitial == AdType.reward {
            callAdMobCompletionHandler(isComplete: true, value: self.rewardedPoints)
            self.rewardedPoints = nil
        } else {
            callAdMobCompletionHandler(isComplete: true, value: nil)
        }
    }
}
