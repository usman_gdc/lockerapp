//
//  FileManager.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 28/06/2021.
//

import Foundation
import UIKit

class FileHadlingManager {
    
    enum DirectoryTypes {
        case photo
        case video
        case file
        case thumbnail
    }
    
    let fileManager: FileManager = FileManager.default
    static let fileDirectoryName: String = "Files"
    
    var documentDirectory: URL? {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
            return nil
        }
        return URL(fileURLWithPath: path)
    }
    
    var photoDirectoryURL: URL? {
        guard let _documentDirectory = documentDirectory else {
            return nil
        }
        let dataPath = _documentDirectory.appendingPathComponent("Photos")
        if !FileManager.default.fileExists(atPath: dataPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        return dataPath
    }
    
    var videoDirectoryURL: URL? {
        guard let _documentDirectory = documentDirectory else {
            return nil
        }
        let dataPath = _documentDirectory.appendingPathComponent("Video")
        if !FileManager.default.fileExists(atPath: dataPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        return dataPath
    }
    
    
    var fileDirectoryURL: URL? {
        guard let _documentDirectory = documentDirectory else {
            return nil
        }
        let dataPath = _documentDirectory.appendingPathComponent(FileHadlingManager.fileDirectoryName)
        
        if !FileManager.default.fileExists(atPath: dataPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        return dataPath
    }
    
    var thumbnailDirectoryURL: URL? {
        guard let _documentDirectory = documentDirectory else {
            return nil
        }
        let dataPath = _documentDirectory.appendingPathComponent("Thumbnail")
        if !FileManager.default.fileExists(atPath: dataPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        return dataPath
    }
    
    func copyFile(file: URL, to directory: FileHadlingManager.DirectoryTypes, byName fileName: String) {
        
        switch directory {
        case .photo:
            guard var _photoUrl = photoDirectoryURL else { return }
            _photoUrl = _photoUrl.appendingPathComponent(fileName)
            self.copyFile(soucrePath: file, destinationPath: _photoUrl)
        case .video:
            guard var _photoUrl = videoDirectoryURL else { return }
            _photoUrl = _photoUrl.appendingPathComponent(fileName)
            self.copyFile(soucrePath: file, destinationPath: _photoUrl)
        case .file:
            guard var _photoUrl = fileDirectoryURL else { return }
            _photoUrl = _photoUrl.appendingPathComponent(fileName)
            self.copyFile(soucrePath: file, destinationPath: _photoUrl)
        case .thumbnail:
            break
        }
        
    }
    
    func copyFile(soucrePath: URL, destinationPath: URL) {
        
        print("Dest: \(destinationPath)")
        print("Src: \(soucrePath)")
        do {
            if fileManager.fileExists(atPath: destinationPath.path) {
                try fileManager.removeItem(at: destinationPath)
            }
            try fileManager.copyItem(atPath: soucrePath.path, toPath: destinationPath.path)
        } catch (let error) {
            print("Cannot copy item at \(soucrePath) to \(destinationPath): \(error)")
        }
    }
    
    func save(image: UIImage, withName name: String) -> URL? {
        guard let data = image.pngData(), let _thumbnailDirectoryURL = thumbnailDirectoryURL else {
            return nil
        }
        let url = _thumbnailDirectoryURL.appendingPathComponent("\(name)")
        do {
            try data.write(to: url)
        } catch {
            return nil
        }
        return url
    }
    
    /// FIles Module
    
    func createDirectoryBy(name: String, inDirectory parentDirectory: String, completion: @escaping(_ url: URL?, _ errorString: String?) -> Void) {
//        guard let _fileDirectoryURL = fileDirectoryURL else {
//            return completion(nil, "Document directory not found")
//        }
        guard let _fileDirectoryURL = documentDirectory else {
            return completion(nil, "Document directory not found")
        }
        let dataPath = _fileDirectoryURL.appendingPathComponent(parentDirectory).appendingPathComponent(name)
        print("Directory path \(dataPath)")
        if !fileManager.fileExists(atPath: dataPath.path) {
            do {
                try fileManager.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
                return completion(dataPath, nil)
            } catch {
                return completion(nil, "\(name) unsuccessful to create directory")
            }
        }
        return completion(nil, "\(name) directory already exist")
    }
    
    func deleteDirectoryBy(name: String) {
        guard let _fileDirectoryURL = fileDirectoryURL else {
            return
        }
        let dataPath = _fileDirectoryURL.appendingPathComponent(name)
        print("Directory path \(dataPath)")
        if !fileManager.fileExists(atPath: dataPath.path) {
            do {
                try fileManager.removeItem(atPath: dataPath.path)
            } catch {
            }
        }
        
    }
    
    func deleteDirectoriesBy(names: [String]) {
        guard let _fileDirectoryURL = fileDirectoryURL else { return }
        
        for name in names {
            let dataPath = _fileDirectoryURL.appendingPathComponent(name)
            print("Directory path \(dataPath)")
            do {
                try fileManager.removeItem(atPath: dataPath.path)
            } catch {
            }
        }
    }
    
    func deleteSingleDirectoryBy(name: String) {
        guard let _fileDirectoryURL = fileDirectoryURL else { return }
            let dataPath = _fileDirectoryURL.appendingPathComponent(name)
            print("Directory path \(dataPath)")
            do {
                try fileManager.removeItem(atPath: dataPath.path)
            } catch {
            }
        
    }
    
    func moveDirectory(from: String, to: String) {
        
        guard let _fileDirectoryURL = documentDirectory else { return }
        
        let source = _fileDirectoryURL.appendingPathComponent(from)
        let dest = _fileDirectoryURL.appendingPathComponent(to).appendingPathComponent(source.lastPathComponent)
        
        print("Directory path from \(source)")
        print("Directory path to \(dest)")
        
        do {
            try fileManager.moveItem(at: source, to: dest)
        } catch {
            print("File not moved")
        }
    }
    
    func moveDirectories(from: [String], to: String) {
        for file in from {
            moveDirectory(from: file, to: to)
        }
    }
}
