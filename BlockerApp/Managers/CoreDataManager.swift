//
//  CoreDataManager.swift
//  iOSApplication
//
//  Created by Muhammd Taha on 09/03/2021.
//

import Foundation
import CoreData

class CoreDataManager {
    
    // MARK: - Core Data stack
    lazy fileprivate var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "LockerApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    static let manager: CoreDataManager = CoreDataManager()
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    private init() {
    }
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

extension CoreDataManager {
    
    //MARK:- Notes
    
    // getnote
    func getAllNotes() throws -> [NotesModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        // we use "predicate" when we have to serach a specific object or specific type of objects.
        //         fetchRequest.predicate = NSPredicate(format: "isArchive == 0")
        fetchRequest.resultType = .dictionaryResultType
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([NotesModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    
    // addnote
    func saveNote(note: NotesModel) throws {
        //        var model = NotesModel()
        //        model.noteId = UUID().uuidString
        //        model.note = String()
        //        model.title = String()
        
        let notesObject = NSEntityDescription.insertNewObject(forEntityName: "Notes", into: context)
        notesObject.setValue(note.noteId, forKey: "noteId")
        notesObject.setValue(note.title, forKey: "title")
        notesObject.setValue(note.note, forKey: "note")
        
        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }
    
    //    func addNote(note: NotesModel) throws {
    //
    //        let notesObject = NSEntityDescription.insertNewObject(forEntityName: "Notes", into: context)
    //        notesObject.setValue(note.noteId, forKey: "noteId")
    //        notesObject.setValue(note.title, forKey: "title")
    //        notesObject.setValue(note.note, forKey: "note")
    //
    //        do {
    //            try context.save()
    //            print("Context Saved" , context)
    //        } catch {
    //            print("Exception while saving")
    //            throw ErrorTypes.notSaved
    //        }
    //    }
    
    
    
    //MARK:- Contacts
    
    
    
    func getAllContacts() throws -> [ContactModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        // we use "predicate" when we have to serach a specific object or specific type of objects.
        //         fetchRequest.predicate = NSPredicate(format: "isArchive == 0")
        fetchRequest.resultType = .dictionaryResultType
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([ContactModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    // save contact
    
    func saveContact(contact: ContactModel)throws {
        var model = ContactModel()
        model.contactId = UUID().uuidString
        model.contactName = String()
        model.contactNumber = String()
        
        let contactObject = NSEntityDescription.insertNewObject(forEntityName: "Contacts", into: context)
        contactObject.setValue(contact.contactId, forKey: "contactId")
        contactObject.setValue(contact.contactName, forKey: "contactName")
        contactObject.setValue(contact.contactNumber, forKey: "contactNumber")
        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }
    
    func importContatcs(contacts: [ContactModel])throws {
        for contact in contacts {
            try? self.saveContact(contact: contact)
        }
    }
    
    func deleteContactBy(contactId: String) throws {
        
        let fetchRequest: NSFetchRequest<Contacts> = Contacts.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "contactId == %@", contactId)
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            print("Error found")
        }
    }
    
    
    
    
    //MARK:- Browser
    
    // addBrowser
    func saveBrowser(browser: BrowserModel) throws {
        let browserObject = NSEntityDescription.insertNewObject(forEntityName: "Browser", into: context)
        browserObject.setValue(browser.browserId, forKey: "browserId")
        browserObject.setValue(browser.browserTitle, forKey: "browserTitle")
        browserObject.setValue(browser.browserUrl, forKey: "browserUrl")
        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }
    
    // getBrowserHistory
    
    func getBrowserHistory() throws -> [BrowserModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Browser")
        fetchRequest.resultType = .dictionaryResultType
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([BrowserModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    func deleteBrowserHistoryBy(browserId: String) throws {
        
        let fetchRequest: NSFetchRequest<Browser> = Browser.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "browserId == %@", browserId)
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            print("Error found")
        }
    }
    
//
    
    
    
    // MARK: - Application
    
    //Save Applications
    
    
    
    
    func saveApplication(app: AppsModel)throws {
        let contactObject = NSEntityDescription.insertNewObject(forEntityName: "Applications", into: context)
        contactObject.setValue(app.bundleId, forKey: "bundleId")
        contactObject.setValue(app.artworkUrl60, forKey: "artworkUrl60")
        contactObject.setValue(app.trackViewUrl, forKey: "trackViewUrl")
        contactObject.setValue(app.trackName, forKey: "trackName")
    
        
        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }
    
    
    //Get Applications
    
    func fetchApps() throws -> [AppsModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Applications")
        fetchRequest.resultType = .dictionaryResultType
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([AppsModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    
    func getAppBybundleId(bundleId: String) -> AppsModel?  {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Applications")
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.predicate = NSPredicate(format: "bundleId == %@", bundleId)
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([AppsModel].self, from: jsonData)
        else {
            print("Error found")
            return nil
        }
        print("Model is ",model)
        return model.first
    }
    
    func deleteAppsBy(bundleId: String) throws {
        
        let fetchRequest: NSFetchRequest<Applications> = Applications.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "bundleId == %@", bundleId)
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            print("Error found")
        }
    }
    
    
    func deleteAllApps() throws {
        
        let fetchRequest: NSFetchRequest<Applications> = Applications.fetchRequest()
      //  fetchRequest.predicate = NSPredicate(format: "bundleId == %@", bundleId)
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            print("Error found")
        }
    }
    
    // Photos
    
    func savePhoto(photo: PhotoModel) throws {
        let contactObject = NSEntityDescription.insertNewObject(forEntityName: "Photo", into: context)
        contactObject.setValue(photo.photoId, forKey: "photoId")
        contactObject.setValue(photo.path, forKey: "path")
        contactObject.setValue(photo.creationDate, forKey: "creationDate")
        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }

    
    func savePhotos(photos: [PhotoModel]) throws {
        
        for photo in photos {
            let contactObject = NSEntityDescription.insertNewObject(forEntityName: "Photos", into: context)
            contactObject.setValue(photo.photoId, forKey: "photoId")
            contactObject.setValue(photo.path, forKey: "path")
            do {
                try context.save()
                print("Context Saved" , context)
            } catch {
                print("Exception while saving")
                throw ErrorTypes.notSaved
            }
        }
    }
    
    func getAllPhotos() throws -> [PhotoModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Photos")
        fetchRequest.resultType = .dictionaryResultType
        let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([PhotoModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    func saveVideos(photos: [VideoModel]) throws {
        
        for photo in photos {
            let contactObject = NSEntityDescription.insertNewObject(forEntityName: "Videos", into: context)
            contactObject.setValue(photo.videoId, forKey: "videoId")
            contactObject.setValue(photo.path, forKey: "path")
            contactObject.setValue(photo.thumbnail, forKey: "thumbnail")
            contactObject.setValue(photo.creationDate, forKey: "creationDate")
            do {
                try context.save()
                print("Context Saved" , context)
            } catch {
                print("Exception while saving")
                throw ErrorTypes.notSaved
            }
        }
    }
    
    func getAllVideos() throws -> [VideoModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Videos")
        fetchRequest.resultType = .dictionaryResultType
        let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([VideoModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        
        return model
    }

    // Files
    
    func saveFile(file: FileModel) throws {
        
        let contactObject = NSEntityDescription.insertNewObject(forEntityName: "Files", into: context)
        contactObject.setValue(file.fileId, forKey: "fileId")
        contactObject.setValue(file.path, forKey: "path")
        contactObject.setValue(file.name, forKey: "name")
        contactObject.setValue(file.parentDirectory, forKey: "parentDirectory")
        contactObject.setValue(NSNumber(integerLiteral: file.isDirectory?.rawValue ?? 1), forKey: "isDirectory")

        do {
            try context.save()
            print("Context Saved" , context)
        } catch {
            print("Exception while saving")
            throw ErrorTypes.notSaved
        }
    }
    
    func getAllFiles() throws -> [FileModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Files")
        fetchRequest.resultType = .dictionaryResultType
        
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([FileModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        print("Model is ",model)
        return model
    }
    
    func getAllFilesBy(directory: String) throws -> [FileModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Files")
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.predicate = NSPredicate(format: "parentDirectory == %@", directory)
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([FileModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        return model
    }
    func getAllFoldersBy(name: String) throws -> [FileModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Files")
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([FileModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        return model
    }
    
    func getFilesBy(category: FileModel.FileType) throws -> [FileModel] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Files")
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.predicate = NSPredicate(format: "isDirectory == %d", category.rawValue)
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([FileModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        return model
    }
    
    func getFilesBy(category: FileModel.FileType, fromDirectory directory: String) throws -> [FileModel] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Files")
        fetchRequest.resultType = .dictionaryResultType
        
        let fileTypePredicate = NSPredicate(format: "isDirectory == %d", category.rawValue)
        let directoryPredicate = NSPredicate(format: "parentDirectory == %@", directory)
        fetchRequest.predicate = NSCompoundPredicate(type: .and, subpredicates: [fileTypePredicate, directoryPredicate])
//        fetchRequest.predicate = NSPredicate(format: "isDirectory == %d", category.rawValue)
        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([FileModel].self, from: jsonData)
        else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        return model
    }
    
    func deleteFile(file: FileModel) {
        
        let fetchRequest: NSFetchRequest<Files> = Files.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "fileId == %@", file.fileId ?? "")
        let objects = try! context.fetch(fetchRequest)
        for obj in objects {
            context.delete(obj)
        }

        do {
            try context.save() // <- remember to put this :)
        } catch {
            
            print("Error found")
        }
    }
    
    func deleteFiles(files: [FileModel]) {
        
        for file in files {
            let fetchRequest: NSFetchRequest<Files> = Files.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "fileId == %@", file.fileId ?? "")
            let objects = try! context.fetch(fetchRequest)
            for obj in objects {
                context.delete(obj)
            }
        }
        
        do {
            try context.save() // <- remember to put this :)
        } catch {
            
            print("Error found")
        }
    }
    
    func deletePhotos(photos: [PhotoModel]) {
        for photo in photos {
            let fetchRequest: NSFetchRequest<Photos> = Photos.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "photoId == %@", photo.photoId ?? "")
            let objects = try! context.fetch(fetchRequest)
            for obj in objects {
                context.delete(obj)
            }
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            
            print("Error found")
        }
    }
    
    func deleteVideos(videos: [VideoModel]) {
        for video in videos {
            let fetchRequest: NSFetchRequest<Videos> = Videos.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "videoId == %@", video.videoId ?? "")
            let objects = try! context.fetch(fetchRequest)
            for obj in objects {
                context.delete(obj)
            }
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            
            print("Error found")
        }
    }
    
    func deletePhotoById(photoId: String) {
            let fetchRequest: NSFetchRequest<Photos> = Photos.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "photoId == %@", photoId )
            let objects = try! context.fetch(fetchRequest)
            for obj in objects {
                context.delete(obj)
            }
        
        do {
            try context.save() // <- remember to put this :)
        } catch {
            
            print("Error found")
        }
    }
    
    
    func deleteVideoById(videoId: String) {
            let fetchRequest: NSFetchRequest<Videos> = Videos.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "videoId == %@", videoId )
            let objects = try! context.fetch(fetchRequest)
            for obj in objects {
                context.delete(obj)
            }
        
        do {
            try context.save() // <- remember to put this :)
        } catch {
            
            print("Error found")
        }
    }
    
    
    
    func updateFile(fileId: String, atPath path: String) throws {
        let fetchRequest: NSFetchRequest<Files> = Files.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "fileId == %@", fileId)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        _first.path = path
        do {
            try context.save() //<-remember to put this
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    
    func updateFile(fileId: String, atDirectory directory: RootDirectory) throws {
        let fetchRequest: NSFetchRequest<Files> = Files.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "fileId == %@", fileId)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        _first.parentDirectory = directory.name
        _first.path = directory.path
        do {
            try context.save() //<-remember to put this
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    
    func updateFileName(fileId: String, name: String) throws {
        let fetchRequest: NSFetchRequest<Files> = Files.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "fileId == %@", fileId)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        _first.name = name
        do {
            try context.save() //<-remember to put this
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    
    func updateNotes(noteId: String, noteTitle: String, note: String) throws {
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "noteId == %@", noteId)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        
        _first.note = note
        _first.title = noteTitle

        do {
            try context.save() //<-remember to put this
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    
    func updateContact(contactId: String , contactName: String, contactNumber: String) throws {
        let fetchRequest: NSFetchRequest<Contacts> = Contacts.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "contactId == %@", contactId)
        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
            print("Error found")
            throw ErrorTypes.notFound
        }
        
        _first.contactName = contactName
        _first.contactNumber = contactNumber

        do {
            try context.save() //<-remember to put this
        } catch {
            print("Error found")
            throw ErrorTypes.notFound
        }
    }
    
    //extension CoreDataManager {
    //
    //MARK:- Notes Functions
    //    func addNote(note: NotesModel) throws {
    //
    //        let notesObject = NSEntityDescription.insertNewObject(forEntityName: "Notes", into: context)
    //        notesObject.setValue(note.noteId, forKey: "noteId")
    //        notesObject.setValue(note.title, forKey: "title")
    //        notesObject.setValue(note.note, forKey: "note")
    //
    //        do {
    //            try context.save()
    //            print("Context Saved" , context)
    //        } catch {
    //            print("Exception while saving")
    //            throw ErrorTypes.notSaved
    //        }
    //    }
    
    //    func getArchiveNotes() throws -> [NotesModel] {
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
    //        fetchRequest.predicate = NSPredicate(format: "isArchive == 1")
    //        fetchRequest.resultType = .dictionaryResultType
    //        guard let results = try? context.fetch(fetchRequest), let jsonData = try? JSONSerialization.data(withJSONObject: results, options: .prettyPrinted), let model = try? JSONDecoder().decode([NotesModel].self, from: jsonData)
    //        else {
    //            print("Error found")
    //            throw ErrorTypes.notFound
    //        }
    //        print("Model is ",model)
    //        return model
    //    }
    //
//        func updateNote(note: NotesModel) throws {
//
//            let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
//            fetchRequest.predicate = NSPredicate(format: "noteId == %@", note.noteId!)
//            guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
//                print("Error found")
//                throw ErrorTypes.notFound
//            }
//            _first.name = note.name
//            _first.email = note.email
//            _first.phoneNumber = note.phoneNumber
//            _first.note = note.note
//            _first.mediaUrl = note.mediaUrl
//            _first.isControl = note.isControl
//            _first.noteAddress = note.noteAddress
//            _first.addressUrl = note.addressUrl
//            _first.detailImageUrl = note.detailImageUrl
//
//            do {
//                try context.save() //<-remember to put this :)
//            } catch {
//                print("Error found")
//                throw ErrorTypes.notFound
//            }
//        }
    //    func updateArchive(note: NotesModel) throws {
    //        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
    //        fetchRequest.predicate = NSPredicate(format: "noteId == %@", note.noteId!)
    //        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
    //            print("Error found")
    //            throw ErrorTypes.notFound
    //        }
    //        _first.isArchive = note.isArchive!
    //        do {
    //            try context.save() //<-remember to put this
    //        } catch {
    //            print("Error found")
    //            throw ErrorTypes.notFound
    //        }
    //    }
    //
    //    func updatearch(noteId : String?, isArchive:Int) throws{
    //        guard let _noteId = noteId else{
    //            return
    //        }
    //        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
    //        fetchRequest.predicate = NSPredicate(format: "noteId == %@", _noteId)
    //        guard let objects = try? context.fetch(fetchRequest), let _first = objects.first else {
    //                       return}
    //        _first.isArchive = Int16(isArchive)
    //        do {
    //                        try context.save() // <- remember to put this :)
    //                    } catch {
    //                        print("Error found")
    //                    }
    //    }
    //}
    
    
}
