//
//  RateAndWriteReviewManager.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 31/08/2021.
//

import Foundation
import StoreKit

class RateAndReviewApp {
    
    static fileprivate var appId: Int = 958625272
    
    class func rateApp() {
        
        if #available(iOS 14.0, *) {
            if let windowScene = UIApplication.shared.windows.first?.windowScene {
                SKStoreReviewController.requestReview(in: windowScene)
            }
        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appId)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    class func reviewApp() {
        guard let productURL = URL(string: "https://apps.apple.com/us/app/charging-play-animation-show/id\(appId)") else { return }
        var components = URLComponents(url: productURL , resolvingAgainstBaseURL: false)
        components?.queryItems = [
            URLQueryItem(name: "action", value: "write-review")
        ]
        guard let writeReviewURL = components?.url else {
            return
        }
        UIApplication.shared.open(writeReviewURL)
    }
    
}
