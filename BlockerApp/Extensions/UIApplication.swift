//
//  UIApplication.swift
//  Development
//
//  Created by Usman Javaid on 30/01/2020.
//  Copyright © 2020 Incubators. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    var safeAreaTopInset: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0.0
        }
        return 0
    }
    
    var safeAreaBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.bottom ?? 0.0
        }
        return 0
    }
    
    var appDelegate: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
//    var topViewController: UIViewController? {
//        get {
//            return self.keyWindow?.rootViewController?.topMostViewController()
//        }
//    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    func changeAppIcon(to name: String?, completion: @escaping( _ errorString: String?) -> Void) {
        guard UIApplication.shared.supportsAlternateIcons else {
            completion("Change app icon feature not available")
            return
        }
        
        UIApplication.shared.setAlternateIconName(name) { (error) in
            
            guard let _ = error else {
                completion(nil)
                return
            }
            completion("Change app icon unsuccessful")
            
        }
    }
    
    
//    func logOutUser() {
//        UserDefaults.standard.user = nil
//        let controller = SigninViewController.initiateFrom(Storybaord: .main)
//        let navigationController = UINavigationController(rootViewController: controller)
//        UIApplication.shared.windows.first?.rootViewController = navigationController
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
//    }
//    
//    func showRootViewController() {
//        let controller = HomeViewController.initiateFrom(Storybaord: .main)
//        let navigationController = UINavigationController(rootViewController: controller)
//        UIApplication.shared.windows.first?.rootViewController = navigationController
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
//    }
}

