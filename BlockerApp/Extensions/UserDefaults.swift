//
//  UserDefault.swift
//  iOSApplication
//
//  Created by Usman Javaid on 12/19/2020.
//  Copyright © 2020 Incubators. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    var appPassCode: AppPassCodeModel? {
        get {
            guard let _data = self.data(forKey: "APPLOCKER_KEYS_APP_PASSCODE"),  let _value = try? JSONDecoder().decode(AppPassCodeModel.self, from: _data) else {
                return nil
            }
            return _value
        }

        set {
            guard let _value = newValue,  let _data = try? JSONEncoder().encode(_value) else {
                self.removeObject(forKey: "APPLOCKER_KEYS_APP_PASSCODE")
                return
            }
            self.setValue(_data, forKey: "APPLOCKER_KEYS_APP_PASSCODE")
            
        }
    }
    var firstSignUp: Bool {
        get {
            self.bool(forKey: "BOOL_FOR_FIRSTSIGNUP")
        }

        set {
            self.setValue(newValue, forKey: "BOOL_FOR_FIRSTSIGNUP")
        }
    }
    
    var appIconName: String? {
        get {
            self.string(forKey: "APPLOCKER_KEYS_APP_APPICON")
        }

        set {
            self.setValue(newValue, forKey: "APPLOCKER_KEYS_APP_APPICON")
        }
    }
    
    var userRatedApp: Bool {
        get {
            self.bool(forKey: "BOOL_FOR_USERRATEDAPP")
        }

        set {
            self.setValue(newValue, forKey: "BOOL_FOR_USERRATEDAPP")
        }
    }
    
    var userRatedAppIcon: Bool {
        get {
            self.bool(forKey: "BOOL_FOR_USERRATEDAPPICON")
        }

        set {
            self.setValue(newValue, forKey: "BOOL_FOR_USERRATEDAPPICON")
        }
    }
    
    var userRatedAppPassword: Int {
        get {
            self.integer(forKey: "BOOL_FOR_USERRATEDAPPPASSWORD")
        }

        set {
            self.setValue(newValue, forKey: "BOOL_FOR_USERRATEDAPPPASSWORD")
        }
    }
    
    var userRatedAppPattern: Int {
        get {
            self.integer(forKey: "BOOL_FOR_USERRATEDAPPPATTERN")
        }

        set {
            self.setValue(newValue, forKey: "BOOL_FOR_USERRATEDAPPPATTERN")
        }
    }
    
    var userRatedAppFaceId: Int {
        get {
            self.integer(forKey: "BOOL_FOR_USERRATEDAPPFACEID")
        }

        set {
            self.setValue(newValue, forKey: "BOOL_FOR_USERRATEDAPPFACEID")
        }
    }
    
    var isPhotosUnlock: Bool {
        get {
            self.bool(forKey: "APPLOCKER_KEYS_LOCK_PHOTOS")
        }

        set {
            self.setValue(newValue, forKey: "APPLOCKER_KEYS_LOCK_PHOTOS")
        }
    }
    
    var isVideosUnlock: Bool {
        get {
            self.bool(forKey: "APPLOCKER_KEYS_LOCK_VIDEOS")
        }

        set {
            self.setValue(newValue, forKey: "APPLOCKER_KEYS_LOCK_VIDEOS")
        }
    }
    
    var isBrowserUnlock: Bool {
        get {
            self.bool(forKey: "APPLOCKER_KEYS_LOCK_BROWSER")
        }

        set {
            self.setValue(newValue, forKey: "APPLOCKER_KEYS_LOCK_BROWSER")
        }
    }
    
}
