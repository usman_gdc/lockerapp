//
//  UIViewController.swift
//  iOSApplication
//
//  Created by Usman Javaid on 14/11/2019.
//  Copyright © 2019 Incubators. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    
    class var storyboardID: String {
        return "\(self)"
    }
    
    static func initiateFrom(Storybaord _storybaord: Storyboard) -> Self {
        return _storybaord.viewController(Class: self)
    }
    
    //MARK:- Properties
    
    var isModal: Bool {

        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController

        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
    
    var hideBackButton: Bool {
        get {
            return self.navigationItem.hidesBackButton
        }
        
        set {
            self.navigationItem.setHidesBackButton(true, animated:true)
            
        }
    }
    
    
    //MARK:- Navigation bar methods
    @discardableResult
    func setRightBarButton(WithTitle title: String, selector: Selector) -> UIButton {
        let button : UIButton = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: button), animated: true)
        self.navigationItem.setRightBarButtonItems([backBarButon], animated: true)
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: button)
        return button
    }
    
    func setRightBarButton(WithTitle title: String, selector: Selector) {
        let button : UIButton = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: button), animated: true)
        self.navigationItem.setRightBarButtonItems([backBarButon], animated: true)
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: button)
    }

    func setRightBarButtonWhite(WithTitle title: String, selector: Selector) {
        let button : UIButton = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: button), animated: true)
        self.navigationItem.setRightBarButtonItems([backBarButon], animated: true)
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: button)
    }

    func setLeftBar(Button button: UIButton) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
        
    func setRightBar(Button button: UIButton) {
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: button), animated: true)
//        self.navigationItem.setRightBarButtonItems([UIBarButtonItem(customView: button)], animated: true)
//        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: button)
    }

    
    func setLeftBarButtonSystemBlue(WithTitle title: String, selector: Selector) {
        let button : UIButton = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItems([backBarButon], animated: false)
    }
    
    func setLeftBarButton(WithTitle title: String, selector: Selector) {
        let button : UIButton = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItems([backBarButon], animated: false)
    }
        
    func setLeftBarButton(WithImage name: String, selector: Selector) {
        let image: UIImage = UIImage(named: name)!
        let button : UIButton = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItems([backBarButon], animated: false)
    }

    @discardableResult
    func setRightBarButton(WithImage name: String, selector: Selector) -> UIButton {
        let image: UIImage = UIImage(named :name)!
        let button : UIButton = UIButton(type: .custom)
        button.leftImage(image: image, renderMode: .automatic)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let barButon: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButtonItems([barButon], animated: true)
        return button
    }
    
    
    //MARK:- Setters
    
    //MARK:- Getters
    
    //MARK:- Helping Methods
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
    
//    func showToastWith(_ message: String?) {
//        guard let _msg = message else {
//            return
//        }
//        let height: CGFloat = (UIScreen.main.bounds.height / 2.0) - 40
//        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
//        progressHUD.mode = MBProgressHUDMode.text
//        progressHUD.detailsLabel.text = _msg
//        progressHUD.margin = 10.0
//        progressHUD.offset.y = height
//        progressHUD.isUserInteractionEnabled = false
//        progressHUD.removeFromSuperViewOnHide = true
//        progressHUD.hide(animated: true, afterDelay: 3.0)
//    }
    
    func isPushed(From _viewContrller: UIViewController.Type) -> Bool {
        guard let _navController = self.navigationController, let previousController = _navController.getPreviousViewController() else {
            return false
        }
        return previousController.isKind(of: _viewContrller)
    }
    
    func isChildOf(parentViewContrller: UIViewController.Type) -> Bool {
        
        if let parentVC = self.parent {
            return parentVC.isKind(of: parentViewContrller)
        }
        return false
    }
    
    func showAlertWithTitle(title: String? = "", message: String?, cancelTitle: String? = "OK", otherTitle: String? = nil, cancelButton: @escaping () -> Void, otherButton: @escaping () -> Void = {}) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionCancel = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
            cancelButton()
        }
        alert.addAction(actionCancel)
        if let _other = otherTitle {
            let actionOther = UIAlertAction(title: _other, style: .default) { (action) in
                otherButton()
            }
            alert.addAction(actionOther)
        }
        self.present(alert, animated: true) {
        }
    }
    
    @discardableResult func popTo(ViewController _viewController: UIViewController.Type) -> Bool {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: _viewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        return false
    }
}
