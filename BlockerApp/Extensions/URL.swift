//
//  URL.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 02/07/2021.
//

import Foundation

extension URL {
    
    var fileName: String {
        return self.deletingPathExtension().lastPathComponent
    }

    var fileExtension: String {
        return self.pathExtension
    }
}
