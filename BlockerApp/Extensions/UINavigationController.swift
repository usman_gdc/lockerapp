//
//  UINavigationController.swift
//  iOSApplication
//
//  Created by Usman Javaid on 19/11/2019.
//  Copyright © 2019 Incubators. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    //MARK:- Properties
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
       return topViewController?.preferredStatusBarStyle ?? .default
    }
    
//    var barHeight: CGFloat {
//        return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
//            (self.navigationController?.navigationBar.frame.height ?? 0.0)
//    }
    
    //MARK:- Methdos
    
    func setTitle(title:String, WithFont font: UIFont, andColor color: UIColor ) {
        self.title = title
        self.setTitleFont(font: font, andColor: color)
    }
    
    func setTitleFont(font: UIFont, andColor color: UIColor ) {
        self.navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: color,
         NSAttributedString.Key.font: font]
    }
    
    func getPreviousViewController() -> UIViewController? {
        let count = viewControllers.count
        guard count > 1 else { return nil }
        return viewControllers[count - 2]
    }
    
    func makeTransparent(tintColor: UIColor = .clear) {
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.tintColor = tintColor
        self.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func setNavigation(tintColor: UIColor = .clear, andBackgroundColor backgroundColor: UIColor = .clear) {
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.barStyle = UIBarStyle.black
        self.navigationBar.barTintColor = backgroundColor
        self.navigationBar.tintColor = tintColor
        self.navigationBar.isTranslucent = false
        self.interactivePopGestureRecognizer?.delegate = nil
    }

    var hideNavigationBar: Bool {
            get {
                guard let _navigationController = self.navigationController else {
                    return false
                }
                return _navigationController.isNavigationBarHidden
            }
    
            set {
                self.setNavigationBarHidden(newValue, animated: true)
            }
        }
    
    
}
