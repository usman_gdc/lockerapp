//
//  FileModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 02/07/2021.
//

import Foundation

class FileModel: Codable {
    
    enum FileType: Int, Codable {
        case file = 0
        case folder = 1
    }
    
    var fileId: String?
    var path: String?
    var name: String?
    var isDirectory: FileType?
    var isSelected: Bool?
    var parentDirectory: String?
}
