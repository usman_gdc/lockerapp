//
//  PhotoModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 28/06/2021.
//

import Foundation

class PhotoModel: Codable {
    var photoId: String?
    var path: String?
    var isSelected: Bool?
    var creationDate: Date?
}
