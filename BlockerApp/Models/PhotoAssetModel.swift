//
//  PhotoAssetModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 28/06/2021.
//

import Foundation
import Photos
import PhotosUI


class PhotoAssetModel {
    var assetId: String = UUID().uuidString
    var isSelected: Bool?
    var asset: PHAsset?
    var image: UIImage?
    
}
