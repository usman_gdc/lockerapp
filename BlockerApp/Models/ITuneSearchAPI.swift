//
//  HideAppsResult.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 10/06/2021.
//

import Foundation

struct ITuneAppsResponeModel: Codable {
    var results: [AppsModel]?
    
}

// MARK: - Result
struct ITuneAppModel: Codable {
    var primaryGenreName: String?
    var artworkUrl100: String?
    var sellerURL: String?
    var artworkUrl512: String?
    var ipadScreenshotUrls: [String]?
    var fileSizeBytes: String?
    var genres, languageCodesISO2A: [String]?
    var artworkUrl60: String?
    var supportedDevices: [String]?
    var trackViewURL: String?
    var resultDescription, bundleID, version: String?
    var artistViewURL: String?
    var userRatingCountForCurrentVersion: Float?
    var isGameCenterEnabled: Bool?
    var genreIDS: [String]?
    var averageUserRatingForCurrentVersion: Double?
    var trackID: Float?
    var minimumOSVersion: String?
    var primaryGenreID: Float?
    var userRatingCount, artistID: Float?
    var artistName: String?
    var price: Float?
    var trackCensoredName, trackName: String?
    var screenshotUrls: [String]?
    var releaseNotes: String?
    var isVppDeviceBasedLicensingEnabled: Bool?
    var sellerName: String?
    var averageUserRating: Double?
    var advisories: [String]?

    enum CodingKeys: String, CodingKey {
        case primaryGenreName, artworkUrl100
        case sellerURL = "sellerUrl"
        case artworkUrl512, ipadScreenshotUrls, fileSizeBytes, genres, languageCodesISO2A, artworkUrl60, supportedDevices
        case trackViewURL = "trackViewUrl"
        case resultDescription = "description"
        case bundleID = "bundleId"
        case version
        case artistViewURL = "artistViewUrl"
        case userRatingCountForCurrentVersion, isGameCenterEnabled
        case genreIDS = "genreIds"
        case averageUserRatingForCurrentVersion
        case trackID = "trackId"
        case minimumOSVersion = "minimumOsVersion"
        case primaryGenreID = "primaryGenreId"
        case userRatingCount
        case artistID = "artistId"
        case artistName, price, trackCensoredName, trackName, screenshotUrls, releaseNotes, isVppDeviceBasedLicensingEnabled, sellerName, averageUserRating, advisories
    }
}

