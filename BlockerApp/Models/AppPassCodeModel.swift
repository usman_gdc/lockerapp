//
//  AppPassCodeModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 21/06/2021.
//

import Foundation


struct AppPassCodeModel: Codable {
    
    enum PassCodeType: Int, Codable {
        case passCode, pattern, faceId
    }
    
    var passCode: String?
    var passCodeType: PassCodeType?
    
}
