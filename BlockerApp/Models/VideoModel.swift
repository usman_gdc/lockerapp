//
//  VideoModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 30/06/2021.
//

import Foundation

class VideoModel: Codable {
    var videoId: String?
    var path: String?
    var thumbnail: String?
    var isSelected: Bool?
    var creationDate: Date?
}
