//
//  AppsModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 22/06/2021.
//

import Foundation

struct AppsModel: Codable {
    
    var bundleId: String?
    var artworkUrl60: String?
    var trackViewUrl: String?
    var trackName: String?
    var isExisted: Bool?
    
//    enum CodingKeys: String, CodingKey {
//        case artworkUrl100
//        case sellerURL = "sellerUrl"
//        case artworkUrl512, ipadScreenshotUrls, fileSizeBytes, genres, languageCodesISO2A, artworkUrl60, supportedDevices
//        case trackViewURL = "trackViewUrl"
//        case resultDescription = "description"
//        case bundleID = "bundleId"
//        case version
//        case artistViewURL = "artistViewUrl"
//        case userRatingCountForCurrentVersion, isGameCenterEnabled
//        case genreIDS = "genreIds"
//        case averageUserRatingForCurrentVersion
//        case trackID = "trackId"
//        case minimumOSVersion = "minimumOsVersion"
//        case primaryGenreID = "primaryGenreId"
//        case userRatingCount
//        case artistID = "artistId"
//        case artistName, price, trackCensoredName, trackName, screenshotUrls, releaseNotes, isVppDeviceBasedLicensingEnabled, sellerName, averageUserRating, advisories
//    }
}



//artworkUrl100    String?    "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/89/bc/2f/89bc2fa9-e372-5c98-5a9d-f27456f89b05/source/100x100bb.jpg"    some
//sellerURL    String?    "http://www.facebook.com/mobile"    some
//artworkUrl512    String?    "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/89/bc/2f/89bc2fa9-e372-5c98-5a9d-f27456f89b05/source/512x512bb.jpg"    some

//artworkUrl60    String?    "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/89/bc/2f/89bc2fa9-e372-5c98-5a9d-f27456f89b05/source/60x60bb.jpg"    some
//trackViewURL    String?    "https://apps.apple.com/us/app/facebook/id284882215?uo=4"
//artistViewURL    String?    "https://apps.apple.com/us/developer/facebook-inc/id284882218?uo=4"    some

//trackID    Float?    284882208
//artistID    Float?    284882208
//artistName    String?    "Facebook, Inc."    some
//trackCensoredName    String?    "Facebook"    some
//trackName    String?    "Facebook"    some
//sellerName    String?    "Facebook, Inc."    some

