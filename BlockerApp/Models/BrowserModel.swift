//
//  BrowserModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 21/06/2021.
//

import Foundation

struct BrowserModel: Codable {
    var browserId: String?
    var browserTitle: String?
    var browserUrl: String?
    
}
