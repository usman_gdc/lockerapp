//
//  ContactModel.swift
//  BlockerApp
//
//  Created by IncubatorMac01 on 17/06/2021.
//

import Foundation

struct ContactModel: Codable {
    var contactId: String?
    var contactName: String?
    var contactNumber: String?
}
